// Create new class
local CLASS = {}


// Some settings for the class
CLASS.DisplayName			= "Prop"
CLASS.WalkSpeed 			= 250
CLASS.CrouchedWalkSpeed 	= 0.2
CLASS.RunSpeed				= 250
CLASS.DuckSpeed				= 0.2
CLASS.DrawTeamRing			= false


// Called by spawn and sets loadout
function CLASS:Loadout(pl)
	// Props don't get anything
end


// Called when player spawns with this class
function CLASS:OnSpawn(pl)
	pl:SetColor( Color(255, 255, 255, 0))
	
	pl.ph_prop = ents.Create("ph_prop")
	pl.ph_prop:SetPos(pl:GetPos())
	pl.ph_prop:SetAngles(pl:GetAngles())
	if pl.propmodel then
    	pl.ph_prop:SetModel(pl.propmodel)
    end
	pl.ph_prop:Spawn()
	pl.ph_prop:SetSolid(SOLID_BBOX)

	pl.ph_prop:SetParent(pl)
	pl.ph_prop:SetOwner(pl)
    pl.ph_prop.max_health = 100


    	pl.ph_prop2 = ents.Create("ph_prop")
    	pl.ph_prop2:SetPos(pl:GetPos()+Vector(0,0,28))
    	pl.ph_prop2:SetAngles(pl:GetAngles())
    	pl.ph_prop2:Spawn()
    	//pl.ph_prop2:SetSolid(SOLID_BBOX)
        pl.ph_prop2:SetModel("models/props_borealis/bluebarrel001.mdl")
    	pl.ph_prop2:SetParent(pl)
    	pl.ph_prop2:SetOwner(pl)
        pl.ph_prop2.max_health = 100
        pl.ph_prop2:SetColor(Color(1,1,1,1))
        pl.ph_prop2:SetMaterial("models/effects/vol_light001")


local hullxymax = math.Round(math.Max( pl.ph_prop2:OBBMaxs().x,  pl.ph_prop2:OBBMaxs().y))
			local hullxymin = hullxymax * -1
			local hullz = math.Round( pl.ph_prop2:OBBMaxs().z)
	pl:SetHull(Vector(hullxymin, hullxymin, 0), Vector(hullxymax, hullxymax, hullz+30))
    			pl:SetHullDuck(Vector(hullxymin, hullxymin, 0), Vector(hullxymax, hullxymax, hullz+30))

            timer.Simple(0.5,function()
                 if IsValid(pl) then
    			    umsg.Start("SetHull", pl)
                				umsg.Long(hullxymax)
                				umsg.Long(hullz)
                				umsg.Short(50)
                			umsg.End()
                			end
            end)
	//pl.ph_prop2:Remove()

end


// Called when a player dies with this class
function CLASS:OnDeath(pl, attacker, dmginfo)
 if(IsValid(pl.ph_prop2))then
      pl.ph_prop2:Remove()
    end
	pl:RemoveProp()
	if IsValid( pl:GetRagdollEntity() ) then
                pl:GetRagdollEntity():Remove()
            end
   
end


// Register
player_class.Register("Prop", CLASS)