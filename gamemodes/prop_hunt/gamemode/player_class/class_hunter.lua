// Create new class
local CLASS = {}


// Some settings for the class
CLASS.DisplayName			= "Hunter"
CLASS.WalkSpeed 			= 230
CLASS.CrouchedWalkSpeed 	= 0.2
CLASS.RunSpeed				= 230
CLASS.DuckSpeed				= 0.2
CLASS.DrawTeamRing			= false


// Called by spawn and sets loadout
function CLASS:Loadout(pl)
	pl:GiveAmmo(64, "Buckshot")
	pl:GiveAmmo(255, "SMG1")
	
	pl:Give("weapon_crowbar")
	pl:Give("weapon_shotgun")
	pl:Give("weapon_smg1")
	pl:Give("item_ar2_grenade")
	
	local cl_defaultweapon = pl:GetInfo("cl_defaultweapon") 
 	 
 	if pl:HasWeapon(cl_defaultweapon) then 
 		pl:SelectWeapon(cl_defaultweapon)
 	end 
end


// Called when player spawns with this class
function CLASS:OnSpawn(pl)
	local unlock_time = math.Clamp(HUNTER_BLINDLOCK_TIME - (CurTime() - GetGlobalFloat("RoundStartTime", 0)), 0, HUNTER_BLINDLOCK_TIME)
	
	//function MyLockFunc()
	//function MyUnlockFunc()
	
	local unblindfunc = function()
		//MyUnblindFunc(pl.Blind(false))
		pl:Blind(false)
	end
	local lockfunc = function()
		//MyLockFunc(pl.Lock())
		pl.Lock(pl)
	end
	local unlockfunc = function()
		//MyUnlockFunc(pl.UnLock())
		pl.UnLock(pl)
	end
	
	if unlock_time > 2 then
		pl:Blind(true)
		
		timer.Simple(unlock_time, unblindfunc)
		
		timer.Simple(2, lockfunc)
		timer.Simple(unlock_time, unlockfunc)
	end
end

local bodies = {}
// Called when a player dies with this class
function CLASS:OnDeath(pl, attacker, dmginfo)

       local ply = pl

        local ent = ents.Create( "prop_physics" )
        ent:SetPos( ply:GetPos() + Vector(0,0,100) )
        ent:SetAngles( Angle( 0, ply:GetAngles().y, 0 ) )
        ent:SetModel( "models/props_c17/gravestone004a.mdl"   )

        ent:Spawn()
        ent:SetCollisionGroup(COLLISION_GROUP_DEBRIS_TRIGGER)
        ent.CollisionGroup = COLLISION_GROUP_DEBRIS_TRIGGER

        ent:SetNWBool("DeadBody", true)
        ent:SetNWString("Name",ply:Nick())

        ent:SetRenderMode( 1 )
        ply.DeadBody = ent
        table.insert( bodies, ent )

        if IsValid( pl:GetRagdollEntity() ) then
            pl:GetRagdollEntity():Remove()
        end
	pl:UnLock()
end


hook.Add( "RoundEnd", "db.ragdollremove", function( ply )

    if table.Count(bodies) > 0 then

	    for k, ent in pairs( bodies ) do

            if not IsValid( ent ) then table.remove( bodies, k ) continue end

                ent:Remove()
                table.remove( bodies, k )
        end
	end

end )


// Register
player_class.Register("Hunter", CLASS)