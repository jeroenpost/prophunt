// Props will not be able to become these models
BANNED_PROP_MODELS = {
    "models/props/cs_assault/dollar.mdl",
    "models/props/cs_assault/money.mdl",
    "models/props/cs_office/snowman_arm.mdl",
    "models/props/cs_office/computer_mouse.mdl",
    "models/props/cs_office/projector_remote.mdl",
    "models/props/CS_militia/reload_bullet_tray.mdl",
    "models/props/cs_militia/reload_bullet_tray.mdl",
    "models/props_vehicles/car002a_physics.mdl",
    "models/props_vehicles/car001b_phy.mdl",
    "models/props_vehicles/car001b_hatchback.mdl",
    "models/props_vehicles/car001a_phy.mdl",
    "models/props_vehicles/car001a_hatchback.mdl",
    "models/props_vehicles/apc001.mdl",
    "models/props_vehicles/car002b_physics.mdl",
    "models/props_vehicles/car003b_physics.mdl",
    "models/props_vehicles/car004a_physics.mdl",
    "models/props_vehicles/car004b_physics.mdl",
    "models/props_vehicles/car005a_physics.mdl",
    "models/props_vehicles/car005b_physics.mdl",
    "models/props_vehicles/tanker001a.mdl",
    "models/props_vehicles/generatortrailer01.mdl",
    "models/props_vehicles/van001a_physics.mdl",

    "models/sims/gm_sponge.mdl", -- <-----------Sponge
    "models/emt props/gbacon.mdl", -- <------------Bacon
    "models/emt props/baconw.mdl", -- <------------Bacon
    "models/emt props/baconstrip.mdl", -- <------------Bacon
    "models/sims/gm_ducky.mdl", -- <-------------Duck, I found it!
    "models/deadrising/cap4/nails.mdl", -- <-------------- Very small
    "models/deadrising/cap6/cards.mdl",
    "models/deadrising/cap6/fries.mdl",
    "models/deadrising/cap6/apple.mdl",

    "models/props/cs_assault/dollar.mdl"
}


// Maximum time (in minutes) for this fretta gamemode (Default: 30)
GAME_TIME = 9999999999999999


// Number of seconds hunters are blinded/locked at the beginning of the map (Default: 30)
HUNTER_BLINDLOCK_TIME = 25


// Health points removed from hunters when they shoot  (Default: 5)
HUNTER_FIRE_PENALTY = 5


// How much health to give back to the Hunter after killing a prop (Default: 20)
HUNTER_KILL_BONUS = 20


// If you loose one of these will be played
// Set blank to disable
LOSS_SOUNDS = {
	"vo/announcer_failure.wav",
	"vo/announcer_you_failed.wav"
}


        // Sound files hunters can taunt with
// You need at least 2 files listed here
HUNTER_TAUNTS = {
    "taunts/you_dont_know_the_power.wav",
    "taunts/you_underestimate_the_power.wav"
}


        // Sound files props can taunt with
// You need at least 2 files listed here
PROP_TAUNTS = {


    "taunts/props/13.mp3",

    "taunts/props/17.mp3",

    "taunts/props/31.mp3",
    "taunts/props/32.mp3",
    "taunts/props/33.mp3",
    "taunts/props/34.mp3",
    "taunts/props/35.mp3",
    --"taunts/you_dont_know_the_power.wav",
    --"taunts/you_underestimate_the_power.wav"
}



// Seconds a player has to wait before they can taunt again (Default: 5)
TAUNT_DELAY = 5


// Rounds played on a map (Default: 10)
ROUNDS_PER_MAP = 1000000


// Time (in seconds) for each round (Default: 300)
ROUND_TIME = 300


// Determains if players should be team swapped every round [0 = No, 1 = Yes] (Default: 1)
SWAP_TEAMS_EVERY_ROUND = 1


// If you win, one of these will be played
// Set blank to disable
VICTORY_SOUNDS = {
	"vo/announcer_success.wav",
	"vo/announcer_victory.wav",
	"vo/announcer_we_succeeded.wav"
}