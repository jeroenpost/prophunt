if not SERVER then return end


--if not CLIENT then return end

util.AddNetworkString("gb_mapvote_startMapVote")
util.AddNetworkString("gb_mapvote_castvote")
util.AddNetworkString("gb_mapvote_getVoteCount")

gb_mapvote.extends = 0
gb_mapvote.currentMap = game.GetMap()
gb_mapvote.round_number = 0
gb_mapvote.votes = {}
gb_mapvote.playerVotes = {}
gb_mapvote.rtv_votes = {}
gb_mapvote.started = false

gb_mapvote.shuffle = function(tab)
    local n, order, res = #tab, {}, {}

    for i=1,n do order[i] = { rnd = math.random(), idx = i } end
    table.sort(order, function(a,b) return a.rnd < b.rnd end)
    for i=1,n do res[i] = tab[order[i].idx] end
    return res
end

gb_mapvote.Initialize = function()

    print("initializing mapvote");

    gb_mapvote.currentMap = game.GetMap()
    gb_mapvote.round_number = 0
    if (gb_mapvote.max_replays >= gb_mapvote.extends) then
        show = 1
    else
        show = 0
    end
    SetGlobalInt("gb_mapvote.ShowStayOnMap", show)
    gb_mapvote.votes = {}
    gb_mapvote.playerVotes = {}
    gb_mapvote.rtv_votes = {}
    gb_mapvote.started = false

    print("Querying maps")
    --db_master:keepAlive()
    if server_id > 49 and server_id < 60 or server_id == 130 or server_id == 131 or server_id == 132 then
            local randommaps = gb_mapvote.shuffle({

             --  "ttt_mc_jondome",
                "ttt_minecraft_haven_feb2015",
                "ttt_minecraftcity_v4_dark",
                "ttt_mc_tiptsky_b4",
                "awp_minecraft_ultimate_v1",
                "dm_minecraft",

                "gg_minecraft",
                "gg_minecraft_dark",
                "gg_minecraft_forts_v1",
                "gg_minecraft_scaz_v2",
                "hoejhus_minecraft_v2",
             --   "ttt_mc_skyislands",
                "ttt_minecraft_b5",
                "ttt_minecraftcity_v4",
                "ttt_minecraft_mythic_b8"
            })
            gb_mapvote.RandomMaps = { randommaps[1],randommaps[2],randommaps[3],randommaps[4],randommaps[5],randommaps[6],randommaps[7],randommaps[8] }


    elseif server_id > 59 and server_id < 70 then
        local randommaps = gb_mapvote.shuffle({

            "ttt_67th_way",
            "ttt_67thway_v2",
            "ttt_67thway_v3",
            "ttt_67thway_v3_new",
            "ttt_67thway_v3_udax",
            "ttt_67thway_v3_winter_b3f",
            "ttt_67thway_v4_fix",
            "ttt_67thway_v5_2014",
            "ttt_67thway_v5_final",
            "ttt_67thway_v6",
            "ttt_67thway_v7_j_l",
            "ttt_67thway_v7_ktt",
            "ttt_68thway_brownie_b"

        })
        gb_mapvote.RandomMaps = { randommaps[1],randommaps[2],randommaps[3],randommaps[4],randommaps[5],randommaps[6],randommaps[7],randommaps[8] }

    elseif server_id == 4 then
        local randommaps = gb_mapvote.shuffle({

            "ttt_krusty_krab",
            "ttt_krusty_krab_a2",
            "ttt_krusty_krab",
            "ttt_krusty_krab_a2",
            "ttt_krusty_krab",
            "ttt_krusty_krab_a2",
            "ttt_krusty_krab",
            "ttt_krusty_krab_a2",

        })
        gb_mapvote.RandomMaps = { randommaps[1],randommaps[2],randommaps[3],randommaps[4],randommaps[5],randommaps[6],randommaps[7],randommaps[8] }


    else
         gb_mapvote.GetRandomMaps()

    end

    print("Updating last played")
    gb_mapvote.UpdateLastPlayed()
    gb_mapvote.UpdateTimesPlayed()
end


gb_mapvote.UpdateLastPlayed = function()


end

gb_mapvote.UpdateTimesPlayed = function()

end

gb_mapvote.GetRandomMaps = function()



    local later_than = os.time() - gb_mapvote.time_between_replay
    local random_map_table = {}

    http.Post( gb_json.settings.url, {['key'] = gb_json.settings.key,
        ['action']="get_random_maps",['table']= gb_mapvote.maps_mysql_table,['number']= tostring(gb_mapvote.number_random_maps) },
    function(data)
        local results = util.JSONToTable( data )
        for k, v in pairs(results) do
            if v.map then
                table.insert(random_map_table, v.map)
                -- Download the image when joining
                resource.AddFile("materials/vgui/ttt_fun_map_icons/" .. v.map .. ".png")
            end
        end

        gb_mapvote.RandomMaps = random_map_table
        timer.Simple(7,function()
            if table.Count(gb_mapvote.RandomMaps) < 5 then
                PrintTable(gb_mapvote.RandomMaps)
                print("Not enough from DB, got "..table.Count(gb_mapvote.RandomMaps))
                gb_mapvote.RandomMaps = gb_mapvote.GetRandomMapsFromFile()
            end
            PrintTable(gb_mapvote.RandomMaps)
        end)
    end);

    return random_map_table
end

gb_mapvote.GetRandomMapsFromFile = function()
    local random_map_table = {}
    local returnMaps = {}
        for k, v in pairs( file.Find( "maps/*.bsp", "GAME" ) ) do
            local name = string.gsub( v, ".bsp", "" )
            table.insert(random_map_table, name)
        end
        -- Shuffle the maps
        local MapCount = #random_map_table
        for i=1,#random_map_table do
            local current = table.remove(random_map_table,math.random(MapCount))
            table.insert(random_map_table,current)
        end
    for k, v in pairs(random_map_table) do
            if k < 10 then
                table.insert(returnMaps, v)
                -- Download the image when joining
                resource.AddFile("materials/vgui/ttt_fun_map_icons/" .. v .. ".png")
            end
        end


    return random_map_table

end

gb_mapvote.RtvSucess = function()
    if table.Count(gb_mapvote.rtv_votes) >= gb_mapvote.rtv_votes_needed() and table.Count(player.GetHumans()) > 1 then
        return true
    end
    return false
end

gb_mapvote.rtv_votes_needed = function()
    return math.ceil(table.Count(player.GetHumans()) * gb_mapvote.rtv_percentage)
end

gb_mapvote.ShouldStartVote = function()
    if gb_mapvote.round_number > gb_mapvote.ttt_rounds_before_vote then
        return true
    end
    return gb_mapvote.RtvSucess()
end

gb_mapvote.UpdateRoundNumber = function()
    gb_mapvote.round_number = gb_mapvote.round_number + 1
end

gb_mapvote.rtv = function(ply)

    if gb_mapvote.RtvSucess() then
        ply:PrintMessage(HUD_PRINTTALK, "A RTV has already passed.")
        return
    end
    if table.HasValue(gb_mapvote.rtv_votes, ply:SteamID()) then
        ply:PrintMessage(HUD_PRINTTALK, "You have already RTV'd.")
        return
    end

    table.insert(gb_mapvote.rtv_votes, ply:SteamID())


    if not gb_mapvote.RtvSucess() then
        message = ply:Name() .. " Wants to change the map! Type !rtv to rock the vote (" .. table.Count(gb_mapvote.rtv_votes) .. "/" .. gb_mapvote.rtv_votes_needed() .. ")."
    else
        message = "RTV Passed! A mapvote will occur after this round.";
    end
    for k, v in pairs(player.GetAll()) do
        v:PrintMessage(HUD_PRINTTALK, message)
    end
end

gb_mapvote.rtvCheckChat = function(ply, text, team)
    if string.find(string.lower(text), "rtv") then
        gb_mapvote.rtv(ply)
    end
end

gb_mapvote.ReceiveVote = function(len, ply)
    if not gb_mapvote.started then return end

    --local data = net.ReadTable()
    local mapID = net.ReadInt(32)
    local userId = net.ReadString()

    if  GetGlobalInt("gb_mapvote.ShowStayOnMap") == 0 and mapID == 99 then return end

    if not (mapID <= table.Count(gb_mapvote.RandomMaps) and mapID >= 1 or mapID == 99) then return end --Make sure the data received is in a valid range.


    didVote = 0
    if gb_mapvote.playerVotes[userId] then
        gb_mapvote.votes[gb_mapvote.playerVotes[userId]] = gb_mapvote.votes[gb_mapvote.playerVotes[userId]] - 1
        didVote = gb_mapvote.playerVotes[userId]
    end

    gb_mapvote.votes[mapID] = (gb_mapvote.votes[mapID] or 0) + 1 --Increment mapID the vote was for, or start tally.
    gb_mapvote.playerVotes[userId] = mapID


    if IsValid(ply) and didVote ~= gb_mapvote.playerVotes[userId] then
        for k, v in pairs(player.GetAll()) do

            if mapID == 99 then
                v:PrintMessage(HUD_PRINTTALK, ply:Nick() .. " voted for: Stay On this map (" .. gb_mapvote.votes[mapID] .. " votes)")
            else
                v:PrintMessage(HUD_PRINTTALK, ply:Nick() .. " voted for: " .. gb_mapvote.RandomMaps[mapID] .. " (" .. gb_mapvote.votes[mapID] .. " votes)")
            end

            --Send it to all players
            net.Start("gb_mapvote_getVoteCount")
            net.WriteString(util.TableToJSON({gb_mapvote.GetWinningMapName(),gb_mapvote.playerVotes}))

            net.Broadcast()
        end
    end

    return
end


gb_mapvote.GetWinningMapName = function()
    winnerID = table.GetWinningKey(gb_mapvote.votes)
    if winnerID == 99 then
        winnerMap = "Stay on this map"
    else
        winnerMap = gb_mapvote.RandomMaps[winnerID]
    end
    --print( winnerMap)
    return winnerMap
end

gb_mapvote.StartVoting = function()
    gb_mapvote.started = true
    net.Start("gb_mapvote_startMapVote")
    net.WriteTable(gb_mapvote.RandomMaps)
    net.Broadcast()
    timer.Simple(gb_mapvote.countdown_seconds, function()
        gb_mapvote.EndVote()
    end )
end

gb_mapvote.EndVote = function()
    if not gb_mapvote.started then return end

    winner = table.GetWinningKey(gb_mapvote.votes)



    if table.Count(gb_mapvote.votes) < 1 or not winner or (not gb_mapvote.RandomMaps[winner] and winner ~= 99) then
        winner = math.floor(math.Rand(1,gb_mapvote.number_random_maps))
    end


    if winner == 99 then
        message = "We stay on this map for another "..(gb_mapvote.number_random_maps).." rounds"
    else
        message = "The winning map is "..gb_mapvote.RandomMaps[winner].."!"
    end

    for k,v in pairs(player.GetAll()) do
        v:PrintMessage( HUD_PRINTTALK, message)
        v:PrintMessage( HUD_PRINTTALK, "("..(gb_mapvote.votes[winner] or 0).."/"..(table.Count(gb_mapvote.playerVotes) or 0)..") votes.")
    end

    if winner == 99 then
        SetGlobalInt(gb_mapvote.RoundsLeftGlobalInt, (gb_mapvote.number_random_maps + 1))
        gb_mapvote.Initialize();
        gb_mapvote.extends = gb_mapvote.extends + 1
    else
        timer.Simple(0.001,  function()
            RunConsoleCommand("changelevel",gb_mapvote.RandomMaps[winner])
        end)
    end
end

gb_mapvote.AdminStartVote = function()
    gb_mapvote.StartVoting()
end

gb_mapvote.CheckAndStart = function()
    if gb_mapvote.ShouldStartVote() then
        if #player.GetAll() == "0" or player.GetAll() == 0 then
            timer.Simple(0.001,  function()
                RunConsoleCommand("changelevel",gb_mapvote.RandomMaps[1])
            end)
            end
        gb_mapvote.StartVoting()

        return true
    end
end

net.Receive("gb_mapvote_castvote", gb_mapvote.ReceiveVote )

-- The Hooks
hook.Add("Initialize", "gb_mapvote_initialize", timer.Simple(5,gb_mapvote.Initialize))
hook.Add("PlayerSay", "gb_mapvote_rtvCheckChat", gb_mapvote.rtvCheckChat)
hook.Add(gb_mapvote.round_preround_hook, "gb_mapvote_UpdateRoundNumber", gb_mapvote.UpdateRoundNumber)
hook.Add(gb_mapvote.round_delay_hook, "gb_mapvote_CheckAndStart", gb_mapvote.CheckAndStart)
hook.Add(gb_mapvote.round_preround_hook, "gb_mapvote_EndVote", gb_mapvote.EndVote)



