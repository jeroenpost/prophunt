hook.Add("TTTBeginRound", "TTTBeginRoundPointshopTD", function()
 			 for k,v in pairs(player.GetAll()) do
				    
                                   

				    --Set Traitor or detective
				    if v.GotTraitor then
					    v.GotTraitor = false
						v:SetRole(ROLE_TRAITOR)
						v:AddCredits(GetConVarNumber("ttt_credits_starting"))
						print(v:Nick().." is set to traitor (pointshop)")
						v:PrintMessage( HUD_PRINTTALK, "[TTT-FUN pointshop] You have been forced to traitor")
                                                SendFullStateUpdate()
					elseif v.GotDetective then
					    v.GotDetective = false
						v:SetRole(ROLE_DETECTIVE)
						v:AddCredits(GetConVarNumber("ttt_credits_starting"))
						print(v:Nick().." is set to detective (pointshop)")
						v:PrintMessage( HUD_PRINTTALK, "[TTT-FUN pointshop] You have been forced to detective")
                                                SendFullStateUpdate()
					end
				    
				    --set DoubleHealth
				    if v.GotDoubleHealth then
				        v.GotDoubleHealth = false
				     	v:SetHealth( v:Health() + 125 );
				     	v:PrintMessage( HUD_PRINTTALK, "[TTT-FUN pointshop] You got extra health")
				     	print(v:Nick().." is set to +125 health (pointshop)")
				    end									
				
				end
                             
				
			end)

function resetBossStatus()
     for k,v in pairs(player.GetAll()) do
         --reset boss status
         v.PS_EquippedThisRound = {}
         v.BoughtBossThisRound = false
         v.IsSpecialPerson = false
         if( v.OldScaleSpecial ) then
            v:SetModelScale(v.OldScaleSpecial, 0.05) 
            v.OldScaleSpecial = false
         end
     end
end


--Remove trails when dead
--function trailremove(ply, ent)
--	if IsValid( ply.TrailPS ) then
--		  SafeRemoveEntity(ply.TrailPS)
--	end
--end
--hook.Add("PlayerDeath", "PS_trailremove", trailremove)

--Make sure person is equipped with all his stuff
function checkPersonPSItems() 
	
	for k,ply in pairs(player.GetAll()) do
		if not IsValid(ply) or not ply:PS_CanPerformAction() then return end
		if TEAM_SPECTATOR != nil and ply:Team() == TEAM_SPECTATOR then return end
		if TEAM_SPEC != nil and ply:Team() == TEAM_SPEC then return end	
		
		for item_id, item in pairs(ply.PS_Items) do
			local ITEM = PS.Items[item_id]
                        if ITEM then
                            local isModel = string.find(ITEM.Category,"odel") 
                            if isModel && isModel > 1 && item.Equipped then
                                if ITEM.WeaponClass then
                                    ply:PS_DropWeaponsSameSlot(ITEM.WeaponClass)
                                    ply:PS_HolsterOthers( item_id )
                                end
                                ITEM:OnEquip(ply, item.Modifiers)
                            end

                            if (item.Equipped and not ply.PS_Equipped[ item_id ]) then
                                    ply.PS_Equipped[ item_id ] = true                                    
                                    ITEM:OnEquip(ply, item.Modifiers)
                            end
                        end
		end
	end
		
end

hook.Add("TTTPrepareRound", "Ps_resetBoosss", function() timer.Simple(0.5, resetBossStatus) end)
hook.Add("TTTPrepareRound", "PS_GiveItems2", function() timer.Simple(3, checkPersonPSItems) end)
--Fix for playermodels
hook.Add("TTTBeginRound", "PS_GiveItems22", function() timer.Simple(2, checkPersonPSItems) end)
hook.Add("TTTBeginRound", "PS_GiveItems23", function() timer.Simple(4, checkPersonPSItems) end)

