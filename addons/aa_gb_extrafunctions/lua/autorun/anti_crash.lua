randomplyCrash = false

hook.Add("Think","AMB_CrashCatcher",function()
        for k, ent in pairs(ents.FindByClass("prop_ragdoll")) do
                if IsValid(ent) then
                        if ent.player_ragdoll then
                                local velo = ent:GetVelocity( ):Length()
                                if velo >= 1350 and velo <= 2099 then
                                        AMB_KillVelocity(ent)
                                        ServerLog("[!CRASHCATCHER!] Caught velocity > 1350 on a ragdoll entity, negating velocity and temporarily disabling motion.\n")
                                        PrintMessage(HUD_PRINTTALK, "[!TTT-FUN CRASHCHECK!] Please don't spaz bodies around.\n")
                                elseif velo >= 2100 then
                                        if CORPSE.GetPlayerNick(ent) ~= "" then
                                            if ent:GetNWBool("body_found", false) == false then

                                                randomplyCrash = table.Random(player.GetAll())
                                                local c = 0
                                                while !randomplyCrash:Alive() do
                                                    randomplyCrash = table.Random(player.GetAll())
                                                    c = c + 1
                                                    if c > 10000 then return end
                                                end
                                                local deadply = player.GetByUniqueID(ent.uqid)
                                                   ent:SetNWBool("body_found", true)
                                                   SCORE:HandleBodyFound(randomplyCrash, deadply )
                                                   CORPSE.SetFound(ent, true)
                                                       SendConfirmedTraitors(GetInnocentFilter(false))

                                            end
                                        end
                                        ent:Remove()
                                        ServerLog("[!CRASHCATCHER!] Caught velocity > 2100 on a ragdoll entity, removing offending ragdoll entity from world.\n")
                                        PrintMessage(HUD_PRINTTALK, "[!TTT-FUN CRASHCHECK!] Removed a body from the game to prevent a crash.\n")
                                end
                        end
                end
        end
end)

function AMB_SetSubPhysMotionEnabled(ent, enable)
   if not IsValid(ent) then return end

   for i=0, ent:GetPhysicsObjectCount()-1 do
      local subphys = ent:GetPhysicsObjectNum(i)
      if IsValid(subphys) then
         subphys:EnableMotion(enable)
         if enable then
            subphys:Wake()
         end
      end
   end
end

function AMB_KillVelocity(ent)
   ent:SetVelocity(vector_origin)

   -- The only truly effective way to prevent all kinds of velocity and
   -- inertia is motion disabling the entire ragdoll for a tick
   -- for non-ragdolls this will do the same for their single physobj
   AMB_SetSubPhysMotionEnabled(ent, false)

   timer.Simple(0, function() AMB_SetSubPhysMotionEnabled(ent, true) end)
end

