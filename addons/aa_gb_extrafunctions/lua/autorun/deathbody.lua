if CLIENT then return end

local noweps = {
	["arrest_stick"] = true,
	["door_ram"] = true,
	["gmod_camera"] = true,
	["gmod_tool"] = true,
	["keys"] = true,
	["lockpick"] = true,
	["med_kit"] = true,
	["pocket"] = true,
	["stunstick"] = true,
	["unarrest_stick"] = true,
	["weapon_keypadchecker"] = true,
	["weapon_physcannon"] = true,
	["weapon_physgun"] = true,
	["weaponchecker"] = true,
	["weapon_fists"] = true,
	["itemstore_pickup"] = true,
}

local bodies = {}
hook.Add( "PlayerDeath", "db.ragdoll", function( ply,int, killer )

        local ent = ents.Create( "prop_physics" )
        ent:SetPos( ply:GetPos() + Vector(0,0,100) )
        ent:SetAngles( Angle( 0, ply:GetAngles().y, 0 ) )
        ent:SetModel( "models/props_c17/gravestone004a.mdl"   )

        ent:Spawn()
        ent:SetCollisionGroup(COLLISION_GROUP_DEBRIS_TRIGGER)
        ent.CollisionGroup = COLLISION_GROUP_DEBRIS_TRIGGER

        ent:SetNWBool("DeadBody", true)
        ent:SetNWString("Name",ply:Nick())

        ent.DecayTime = CurTime() + 120

        ent:SetRenderMode( 1 )
        ply.DeadBody = ent
        table.insert( bodies, ent )

        if IsValid( ply:GetRagdollEntity() ) then
            ply:GetRagdollEntity():Remove()
        end


end )

hook.Add( "RoundEnd", "db.sragdollremoveqweada", function( ply )

    if table.Count(bodies) > 0 then

	    for k, ent in pairs( bodies ) do

            if not IsValid( ent ) then table.remove( bodies, k ) continue end

                ent:Remove()
                table.remove( bodies, k )
        end
	end

end )
