AddCSLuaFile()

--Check if ply is admin
function gb_isAdmin(player)
    if IsValid(ply) then 
        if player:IsUserGroup("superadmin") or 
           player:IsUserGroup("head_admin") or
           player:IsUserGroup("sr_admin")   or
           player:IsUserGroup("admin")      or
           player:IsUserGroup("jr_admin")   or
           player:IsUserGroup("manager")    or
           player:IsUserGroup("super_donator") 
          then
           return true
          else
           return false
         end
        
    end
    return false
end



if SERVER then


-- Allow an admin to talk while death
function playerAdminTalk( listener, talker )    
    if IsValid(talker) and talker.IsAbleToTalkWhileDeath then
        return true
    end 
end

hook.Add("PlayerCanHearPlayersVoice","enableAdminTalkingDeath", playerAdminTalk)

   
 util.AddNetworkString("admin_getDamageLogFromServer")
 util.AddNetworkString("admin_getDamageLog") 

  net.Receive( "admin_getDamageLogFromServer", function(len,ply)
    
     damagelog = ''
     good = "<span style='color:green;'>"
     hgood = "<span style='color:orange;'>"
     bad = "<span style='color:red;'>"
     lelse = "<span style='color:blue;'>"
     lend = "</span><br/>"
 
     for k, txt in ipairs(GAMEMODE.DamageLog) do

        killl = string.find( txt, "KILL")
		
		if killl == 12 then
		 good = "<div style='color:white;background:green;font-weight:bold;margin:3px;'>"
		 hgood = "<div style='color:white;background:#698705;font-weight:bold;margin:3px;'>"
		 bad = "<div style='color:white;background:red;font-weight:bold;margin:3px;'>"
		 lelse = "<div style='color:#050587;font-weight:bold;margin:3px;'>"
		 lend = "</div>"
		else
		 good = "<span style='color:#1E5520;'>"
		 hgood = "<span style='color:#53551E;'>"
		 bad = "<span style='color:#551E1E;'>"
		 lelse = "<span style='color:#1E1E55;'>"
		 lend = "</span><br/>"
		end
        propkill = string.find( txt, "%<something%/world%>")
        innocent = string.find(  txt, "%[innocent%]"); 
        if innocent then innocent2 = string.find( txt, "%[innocent%]", innocent+10) end
        detective = string.find( txt, "%[detective%]");
        if detective then detective2 = string.find( txt, "%[detective%]", detective+12) end
        traitor = string.find( txt, "%[traitor%]");
        if traitor then traitor2 = string.find( txt, "%[traitor%]", traitor+9) end
        
        if     propkill                 then damagelog = damagelog..good..string.gsub(string.gsub(txt,">","@]"),"<","@[")..lend 
        elseif innocent  and innocent2  then damagelog = damagelog..bad..txt..lend  
        elseif traitor   and traitor2   then damagelog = damagelog..bad..txt..lend  
        elseif detective and detective2 then damagelog = damagelog..bad..txt..lend  
        elseif innocent  and detective  then damagelog = damagelog..bad..txt..lend 
        elseif innocent  and traitor    and (innocent > traitor) then damagelog = damagelog..good..txt..lend 
        elseif innocent  and traitor    and (innocent < traitor) then  damagelog = damagelog..hgood..txt..lend         
        elseif traitor   and detective  and (detective > traitor) then damagelog = damagelog..good..txt..lend 
        elseif traitor   and detective  and (detective < traitor) then  damagelog = damagelog..hgood..txt..lend  
        else damagelog = damagelog..lelse..txt..lend end
        
                
	
     end

     -- Hook for damagelogs
     hook.Add("addToDamageLog","gb_dlogs_adddamagelost",function(message)
         DamageLog( message )
     end)
     
   
     damagelog = "<div style='background:white;color:black;padding:10px;font-size:11px;font-family:Arial,sans'>"..damagelog.."<br/><br/>\n*** Damage log end.</div>"
     print('Sending the damagelog to '..ply:Nick())
     net.Start("admin_getDamageLog")
         net.WriteTable({ damagelog, damagelog  }) 
     net.Send(ply) 
  end)

end


if CLIENT then

    dlogsHtml = {}
    dlogFrame = {}

      net.Receive( "admin_getDamageLog", function()
                 print('receiving damagelogs')
                 data = net.ReadTable()
                  if IsValid(dlogsHtml) then
                     dlogsHtml:SetHTML( data[1] )
                  else
                  print("Dlogs windows is closed")
                   end
                end)  
   

    function gb_print_damagelog()
      if (not IsValid(ply)) or gb_isAdmin(ply) or GetRoundState() != ROUND_ACTIVE then

            
	
           dlogFrame = vgui.Create("DFrame") 	
           dlogFrame:SetPos(200, 100)              
           dlogFrame:SetSize(790, 600)    
           dlogFrame:SetTitle("TTT-FUN Damage Logs viewer") 
           dlogFrame:SetVisible(true)             
           dlogFrame:SetDraggable(true)      
           dlogFrame:ShowCloseButton(true)   
           dlogFrame:SetScreenLock(true)    
           dlogFrame:Center()
           dlogFrame:SetBackgroundBlur( true )	
           dlogFrame.Paint = function()
              draw.RoundedBox( 8, 0, 0, dlogFrame:GetWide(), dlogFrame:GetTall(), Color( 0, 0, 0, 252 ) )
           end
          dlogFrame:MakePopup()    

        dlogsHtml = vgui.Create( "HTML", dlogFrame )
        dlogsHtml:SetPos( 20, 20 )
        dlogsHtml:SetSize( 750, 560 )
        --dlogsHtml:SetHTML( "<div style='color:white'>Loading logs....</div>" )

           

          net.Start("admin_getDamageLogFromServer")
	    net.WriteEntity( ply ) --Dummy thing
          net.SendToServer()

      end
    end
concommand.Add("gb_print_damagelog", gb_print_damagelog)

end


