
local CATEGORY_NAME = "TTT Admin"

--[[corpse_remove][removes the corpse given.]
@param  {[Ragdoll]} corpse [The corpse to be removed.]
--]]
function corpse_remove(corpse)
    CORPSE.SetFound(corpse, false)
    if string.find(corpse:GetModel(), "zm_", 6, true) then
        corpse:Remove()
    elseif corpse.player_ragdoll then
        corpse:Remove()
    end
end

--[[corpse_identify][identifies the given corpse.]
@param  {[Ragdoll]} corpse [The corpse to be identified.]
--]]
function corpse_identify(corpse)
    if corpse then
        local ply = player.GetByUniqueID(corpse.uqid)
        ply:SetNWBool("body_found", true)
        CORPSE.SetFound(corpse, true)
    end
end
--[End]----------------------------------------------------------------------------------------

function corpse_find(v)
    for _, ent in pairs( ents.FindByClass( "prop_ragdoll" )) do
        if ent.uqid == v:UniqueID() and IsValid(ent) then
            return ent or false
        end
    end
end


------------------------------ UserTime ------------------------------
function ulx.onlinetime( calling_ply, target_plys, hours )
    
	amount = ((hours * 60) * 60)
	 
    for i=1, #target_plys do
		--target_plys[ i ]:ConCommand("setPlayerTime "..amount)
		target_plys[ i ]:SetUTime( amount )
		target_plys[ i ]:SetUTimeStart( CurTime() )
		SaveUserData( target_plys[ i ] )
		target_plys[i]:PS_Notify( "An admin set your time to "..hours.." hours" )
    end
    
ulx.fancyLogAdmin( calling_ply, "#A set the time for #T to #i hours", target_plys, hours )
end
local onlinetime = ulx.command( "TTT Fun", "ulx onlinetime", ulx.onlinetime, "!onlinetime" )
onlinetime:addParam{ type=ULib.cmds.PlayersArg }
onlinetime:addParam{ type=ULib.cmds.NumArg, min=1, max = 12096000, default=86400, hint="Time in Hours", ULib.cmds.round }
onlinetime:defaultAccess( ULib.ACCESS_SUPERADMIN )
onlinetime:help( "Changes the target(s) Online Time." )


 ------------------------------ DeathTalking Admin ------------------------------
function ulx.talkwhiledeath( calling_ply, target_plys, seconds )
	 
    for i=1, #target_plys do
		--target_plys[ i ]:ConCommand("setPlayerTime "..amount)
		target_plys[ i ].IsAbleToTalkWhileDeath = true
		target_plys[i]:PS_Notify( "You are able to talk for "..seconds.." seconds while death" )
                timer.Simple(seconds, function()
                    if IsValid(target_plys[ i ]) then
                        target_plys[ i ].IsAbleToTalkWhileDeath = false
                    end
                end)
    end
    
ulx.fancyLogAdmin( calling_ply, "#A set #T to be able to talk to the living for #i seconds", target_plys, seconds )
end
local talkwhiledeath = ulx.command( "TTT Fun", "ulx talkwhiledeath", ulx.talkwhiledeath, "!talkwhiledeath" )
talkwhiledeath:addParam{ type=ULib.cmds.PlayersArg }
talkwhiledeath:addParam{ type=ULib.cmds.NumArg, min=15, max = 300, default=60, hint="Time in seconds", ULib.cmds.round }
talkwhiledeath:defaultAccess( ULib.ACCESS_SUPERADMIN )
talkwhiledeath:help( "Makes you able talk while death" )

----- DAMAGELOG -----

function ulx.dlog( calling_ply )
    calling_ply:ConCommand("gb_print_damagelog") -- This runs ttt_print_damagelog on the person who called the command (pressed the button or said !dlogs).


    ulx.fancyLogAdmin( calling_ply, "#A is viewing the damage logs.", command, target_plys )
end
local dlog= ulx.command( "TTT Fun", "ulx dlog", ulx.dlog, "!dlog" ) -- Makes the command be called dlogs and puts it in the Utility category.
dlog:defaultAccess( ULib.ACCESS_ADMIN )
dlog:help( "Shows the damage logs." )

----- DAMAGELOG -----

function ulx.dlogs( calling_ply )
	--calling_ply:ConCommand("gb_print_damagelog") -- This runs ttt_print_damagelog on the person who called the command (pressed the button or said !dlogs).
    calling_ply:ConCommand("ttt_show_damagelog")

	ulx.fancyLogAdmin( calling_ply, "#A is viewing the damage logs.", command, target_plys )
end
local dlogs= ulx.command( "TTT Fun", "ulx dlogs", ulx.dlogs, "!dlogs" ) -- Makes the command be called dlogs and puts it in the Utility category.
dlogs:defaultAccess( ULib.ACCESS_ADMIN )
dlogs:help( "Shows the damage logs." )


----- DAMAGELOG -----

function ulx.dlogs2( calling_ply )
    --calling_ply:ConCommand("gb_print_damagelog") -- This runs ttt_print_damagelog on the person who called the command (pressed the button or said !dlogs).
    calling_ply:ConCommand("ttt_show_lastdamagelog")
    ulx.fancyLogAdmin( calling_ply, "#A is viewing the damage logs of last round.", command, target_plys )
end
local dlogs2= ulx.command( "TTT Fun", "ulx lastdlogs", ulx.dlogs, "!lastdlogs" ) -- Makes the command be called dlogs and puts it in the Utility category.
dlogs2:defaultAccess( ULib.ACCESS_ADMIN )
dlogs2:help( "Shows the last rounds damage logs." )

 ------------------------------ pspoint ------------------------------
function ulx.pspoint( calling_ply, target_plys, amount )
    
    for i=1, #target_plys do
		target_plys[i]:PS_GivePoints(amount)
	    target_plys[i]:PS_Notify( "An admin gave you "..amount.." points" )
        --target_plys[ i ]:AddCredits(amount)
    end
    
ulx.fancyLogAdmin( calling_ply, true, "#A has given #T #i Points", target_plys, amount )
end
local acred = ulx.command("TTT Fun", "ulx pspoints", ulx.pspoint, "!pspoints")
acred:addParam{ type=ULib.cmds.PlayersArg }
acred:addParam{ type=ULib.cmds.NumArg, hint="Pointshop Points", ULib.cmds.round }
acred:defaultAccess( ULib.ACCESS_ADMIN )
acred:help( "Gives the target(s) Pointshop points." )

----- AdminCheat -----

function ulx.adminch( calling_ply, target_plys )
	-- calling_ply:ConCommand("adminch") 
	if calling_ply:IsValid() then
	  print( "--- "..calling_ply:Nick().." used the admincheat and gave it to:")
	else
	  print( "--- Console used the admincheat and gave it to:")
	end
	for _,v in pairs(target_plys) do
	    v.BoughtCheater = true
        v:ConCommand("adminch")  
        print( v:Nick().."")                        
    end
    print("end ---")
	-- ulx.fancyLogAdmin( calling_ply, "#A is viewing the damage logs.", command, target_plys )
end
local adminch= ulx.command( "TTT Fun", "ulx admincheat", ulx.adminch, "!admincheat" )
adminch:addParam{ type=ULib.cmds.PlayersArg } 
adminch:defaultAccess( ULib.ACCESS_SUPERADMIN )
adminch:help( "Give some superpowers to someone :-). Use: Hold B for settings, hold X for aimbot. T for triggerbot." )


---- GIVEWEAPON
function ulx.giveweapon( calling_ply, target_plys, weapon )
	
	
	local affected_plys = {}
	for i=1, #target_plys do
		local v = target_plys[ i ]
		
		if not v:Alive() then
		ULib.tsayError( calling_ply, v:Nick() .. " is dead", true )
		
		else
		v:Give(weapon)
		table.insert( affected_plys, v )
		end
	end
	ulx.fancyLogAdmin( calling_ply, "#A gave #T weapon #s", affected_plys, weapon )
end
	
	
local giveweapon = ulx.command( "TTT Fun", "ulx giveweapon", ulx.giveweapon, "!giveweapon" )
giveweapon:addParam{ type=ULib.cmds.PlayersArg }
giveweapon:addParam{ type=ULib.cmds.StringArg, hint="weapon_nyangun" }
giveweapon:defaultAccess( ULib.ACCESS_ADMIN )
giveweapon:help( "Give a player a weapon - !giveweapon  " )

---- SHOWCEATER
function ulx.showcheaters( calling_ply )
    print( "--- Cheaters from pointshop:")
	for _,v in pairs(player.GetAll()) do
	    if v.BoughtCheater == true then
	ULib.tsay( calling_ply, v:Nick().." has cheater", true )
        print( v:Nick().."\t\t\tYES")   
        else
		print( v:Nick().."\t\t\\tNO")   
	    end
    end
	 print( "END Cheaters from pointshop --- ")
	
end
		
local showcheaters = ulx.command( "TTT Fun", "ulx showcheaters", ulx.showcheaters, "!showcheaters" )
showcheaters:defaultAccess( ULib.ACCESS_ADMIN )
showcheaters:help( "Print list of cheaters in console (bought from pointshop)" )


-- Give Ent


function ulx.giveent( calling_ply, target_plys, classname, params )
    
    for i=1, #target_plys do
        classname = classname:lower()
		newEnt = ents.Create( classname )

		-- Make sure it's a valid ent
		if not newEnt or not newEnt:IsValid() then
			print("Ent is not valid")
			return
		end    
    
		local v = target_plys[ i ]

        local trace = v:GetEyeTrace()
		local vector = trace.HitPos
		vector.z = vector.z + 20

		newEnt:SetPos( vector ) -- Note that the position can be overridden by the user's flags

		params:gsub( "([%w%p]+)\"?:\"?([%w%p]+)", function( key, value )
			newEnt:SetKeyValue( key, value )
		end ) 

		newEnt:Spawn()
		newEnt:Activate()

		undo.Create( "ulx_ent" )
			undo.AddEntity( newEnt )
			undo.SetPlayer( v )
		undo.Finish()
    end 
	if not params or params == "" then
	--	ulx.fancyLogAdmin( calling_ply, "#A created ent #s", classname )
	else
	--	ulx.fancyLogAdmin( calling_ply, "#A created ent #s with params #s", classname, params )
	end
end

entListgg = {
"gb_pika","gb_spyro","gb_god","gb_obama","gb_sycto","gb_greenblack","gb_tails","gb_sonic","gb_police","gb_osama",

"npc_alyx","npc_antlion","npc_antlion_template_maker","npc_antlionguard","npc_barnacle","npc_barney","npc_breen","npc_citizen","npc_combine_camera","npc_combine_s","npc_combinedropship","npc_combinegunship","npc_crabsynth","npc_cranedriver","npc_crow","npc_cscanner","npc_dog","npc_eli","npc_fastzombie","npc_fisherman (not available in Hammer by default)","npc_gman","npc_headcrab","npc_headcrab_black","npc_headcrab_fast","npc_helicopter","npc_ichthyosaur","npc_kleiner","npc_manhack","npc_metropolice","npc_monk","npc_mortarsynth","npc_mossman","npc_pigeon","npc_poisonzombie","npc_rollermine","npc_seagull","npc_sniper","npc_stalker","npc_strider","npc_turret_ceiling","npc_turret_floor","npc_turret_ground","npc_vortigaunt","npc_zombie","npc_zombie_torso"
}

local giveent = ulx.command(  "TTT Fun", "ulx giveent", ulx.giveent, "!giveent" )
giveent:addParam{ type=ULib.cmds.PlayersArg }
giveent:addParam{ type=ULib.cmds.StringArg, hint="npc_alyx", completes=entListgg }
giveent:addParam{ type=ULib.cmds.StringArg, hint="additionalequipment:weapon_zm_pistol", ULib.cmds.takeRestOfLine, ULib.cmds.optional }
giveent:defaultAccess( ULib.ACCESS_SUPERADMIN )
giveent:help( "Spawn an ent, separate flag and value with ':'." )


-- Rocket

local directions = {"up", "down", "left", "right", "forward", "back", "u", "d", "l", "r", "f", "b"}
 
function ulx.rocket( calling_ply, target_plys, string_arg )
       
        for _, v in ipairs( target_plys ) do
                if not v:Alive() then
                        ULib.tsay( calling_ply, v:Nick() .. " is dead!", true )
                        return
                end
                if v.jail then
                        ULib.tsay( calling_ply, v:Nick() .. " is in jail", true )
                        return
                end
                if v.ragdoll then
                        ULib.tsay( calling_ply, v:Nick() .. " is a ragdoll", true )
                        return
                end    
 
                if v:InVehicle() then
                        local vehicle = v:GetParent()
                        v:ExitVehicle()
                end
                v:SetMoveType(MOVETYPE_WALK)
                tcolor = team.GetColor( v:Team()  )
                local trail = util.SpriteTrail(v, 0, Color(tcolor.r,tcolor.g,tcolor.b), false, 60, 20, 4, 1/(60+20)*0.5, "trails/smoke.vmt")                           
               
                if( string_arg == "up" or string_arg == "u" ) then
                        v:SetVelocity(Vector(0, 0, 2048))
                elseif ( string_arg == "down" or string_arg == "d" ) then
                        v:SetVelocity(Vector(0, 0, -2048))
                elseif ( string_arg == "left" or string_arg == "l" ) then
                        v:SetVelocity(v:GetLeft() * 2048)
                elseif ( string_arg == "right" or string_arg == "r" ) then
                        v:SetVelocity(v:GetRight() * 2048)
                elseif ( string_arg == "forward" or string_arg == "f" ) then
                        v:SetVelocity(v:GetForward() * 2048)
                elseif ( string_arg == "back" or string_arg == "b" ) then
                        v:SetVelocity(v:GetForward() * -2048)
                end
                       
               
                timer.Simple(2.5, function()
                        local Position = v:GetPos()            
                        local Effect = EffectData()
                        Effect:SetOrigin(Position)
                        Effect:SetStart(Position)
                        Effect:SetMagnitude(512)
                        Effect:SetScale(128)
                        util.Effect("Explosion", Effect)
                        timer.Simple(0.1, function()
                                v:KillSilent()
                                trail:Remove()
                                local corpse = corpse_find(v)
                                if corpse then
                                    corpse_identify(corpse)
                                    corpse_remove(corpse)
                                end
                        end)
                end)
        end
        ulx.fancyLogAdmin( calling_ply, "#A turned #T into a rocket!", target_plys )
end
 
local rocket = ulx.command( "Fun", "ulx rocket", ulx.rocket, "!rocket" )
rocket:addParam{ type=ULib.cmds.PlayersArg }
rocket:addParam{ type=ULib.cmds.StringArg, completes=directions, default="up", hint="direction", error="invalid direction \"%s\" specified", ULib.cmds.restrictToCompletes }
rocket:defaultAccess( ULib.ACCESS_ADMIN )
rocket:help( "Rocket players into the air" )


---[Prevent win]-------------------------------------------------------------------------
function ulx.preventwin( calling_ply, should_prevwin )
    if not GetConVarString("gamemode") == "terrortown" then ULib.tsayError( calling_ply, gamemode_error, true ) else
        if should_prevwin then
            ULib.consoleCommand( "ttt_debug_preventwin 0" .. "\n" )
            ulx.fancyLogAdmin( calling_ply, "#A allowed the round to end as normal." )
        else
            ULib.consoleCommand( "ttt_debug_preventwin 1" .. "\n" )
            ulx.fancyLogAdmin( calling_ply, "#A prevented the round from ending untill timeout." )
        end
    end
end
local preventwin = ulx.command( CATEGORY_NAME, "ulx prevwin", ulx.preventwin )
preventwin:defaultAccess( ULib.ACCESS_SUPERADMIN )
preventwin:addParam{ type=ULib.cmds.BoolArg, invisible=true }
preventwin:setOpposite( "ulx allowwin", {_, true} )
preventwin:help( "Toggles the prevention of winning." )
---[End]----------------------------------------------------------------------------------------


---[Round Restart]-------------------------------------------------------------------------
function ulx.roundrestart( calling_ply )
    if not GetConVarString("gamemode") == "terrortown" then ULib.tsayError( calling_ply, gamemode_error, true ) else
        ULib.consoleCommand( "ttt_roundrestart" .. "\n" )
        ulx.fancyLogAdmin( calling_ply, "#A has restarted the round." )
    end
end
local restartround = ulx.command( CATEGORY_NAME, "ulx roundrestart", ulx.roundrestart )
restartround:defaultAccess( ULib.ACCESS_SUPERADMIN )
restartround:help( "Restarts the round." )
---[End]----------------------------------------------------------------------------------------



