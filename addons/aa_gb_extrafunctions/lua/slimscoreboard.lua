

include("sscoreboard_config.lua")
local Config = SlimScoreboardConfig

if SERVER then
	AddCSLuaFile()
	AddCSLuaFile("sscoreboard_config.lua")

	CreateConVar("ss_ver", 1, FCVAR_NOTIFY)

	return
end

hook.Add("Tick", "SSTick", function()
	Config = SlimScoreboardConfig
end)

/*------------------------
   Fonts and materials
--------------------------*/

surface.CreateFont("ScoreboardTitle", {
   font  = "Helvetica",
   size  = 63,
   weight   = 800
})

surface.CreateFont("ScoreboardTitleSmall", {
   font  = "Helvetica",
   size  = 23,
   weight   = 800
})

surface.CreateFont("ScoreboardText", {
   font  = "Helvetica",
   size  = 26,
   weight   = 800
})

surface.CreateFont("ScoreboardTextSmall", {
   font  = "Helvetica",
   size  = 20,
   weight   = 800
})

local MatGradientUp = Material("vgui/gradient-u")
local MatGradientDown = Material("vgui/gradient-d")

/*------------------------------------
	Useful functions
--------------------------------------*/

local function DrawBox(x, y, w, h, col)
	surface.SetDrawColor(col)
	surface.DrawRect(x, y, w, h)

	surface.SetDrawColor(255, 255, 255, 50)
	surface.DrawLine(x+1, y+1, x+w-2, y+1)
	surface.SetDrawColor(255, 255, 255, 20)
	surface.DrawLine(x+w-2, y+1, x+w-2, y+h-1)

	surface.SetDrawColor(0, 0, 0, 150)
	surface.DrawOutlinedRect(x, y, w, h)

	surface.SetDrawColor(0, 0, 0, 80)
	surface.SetMaterial(MatGradientDown)
	surface.DrawTexturedRect(x, y+h*0.7, w, h*0.3)
end

local function DrawBoxUp(x, y, w, h, col)
	surface.SetDrawColor(col)
	surface.DrawRect(x, y, w, h)

	surface.SetDrawColor(255, 255, 255, 50)
	surface.DrawLine(x+1, y+1, x+w-2, y+1)
	surface.SetDrawColor(255, 255, 255, 20)
	surface.DrawLine(x+w-2, y+1, x+w-2, y+h-1)

	surface.SetDrawColor(0, 0, 0, 150)
	surface.DrawOutlinedRect(x, y, w, h)

	surface.SetDrawColor(0, 0, 0, 80)
	surface.SetMaterial(MatGradientUp)
	surface.DrawTexturedRect(x, y, w, h*0.3)
end

local function DrawBar(x, y, w, h, text, percent, max, color)
	surface.SetDrawColor(color.r*0.3, color.g*0.3, color.b*0.3, color.a)
	surface.DrawRect(x, y, w, h)

	surface.SetDrawColor(color.r, color.g, color.b, color.a)
	surface.DrawRect(x, y, w * math.Clamp(percent/max, 0, 1), h)

	surface.SetDrawColor(color.r*0.8, color.g*0.8, color.b*0.8, color.a)
	surface.SetMaterial(MatGradientUp)
	surface.DrawTexturedRect(x, y, w * math.Clamp(percent/max, 0, 1), h)

	surface.SetDrawColor(0, 0, 0, color.a*0.8)
	surface.SetMaterial(MatGradientDown)
	surface.DrawTexturedRect(x, y, w, h)

	surface.SetDrawColor(color.r*0.3, color.g*0.3, color.b*0.3, color.a)
	surface.DrawOutlinedRect(x, y, w, h)

	draw.DrawText(math.Clamp(math.floor(percent+0.5), 0, 999999) .. "%", "ScoreboardTitleSmall", x + w/2, y + 3, Color(255, 255, 255), TEXT_ALIGN_CENTER)
end


local function FromCapital(text)
	return text:sub(1,1):upper() .. text:sub(2)
end

local function MulColor(col, fac)
	return Color(col.r*fac, col.g*fac, col.b*fac)
end

function FormatTime(timetable, type)
	if type == 0 then
		return string.format("%02i:%02i:%02i", math.floor(timetable.h/24), timetable.h%24, timetable.m)
	elseif type == 1 then
		return string.format("%02i:%02i:%02i:%02i", math.floor(timetable.h/24), timetable.h%24, timetable.m, math.floor(timetable.s)%60)
	elseif type == 2 then
		if timetable.h > 24 then
			local days = math.floor(timetable.h/24)
			if days > 7 then
				return string.format("%02iw %02id %02ih %02im", math.floor(days/7), days%7, timetable.h%24, timetable.m)
			else
				return string.format("%02id %02ih %02im", days, timetable.h%24, timetable.m)
			end
		else
			return string.format("%02ih %02im", timetable.h, timetable.m)
		end
	end
end

/*------------------------------------
	Buttons
--------------------------------------*/

local function MakeButton(parent)
	local btn = parent:Add("DButton")

	btn.Color = Color(230, 230, 230)

	function btn:Paint(w, h)
		if self:IsDown() then
			DrawBoxUp(0, 0, w, h, MulColor(self.Color, 0.8))
		elseif self:IsHovered() then
			DrawBox(0, 0, w, h, MulColor(self.Color, 1.2))
		else
			DrawBox(0, 0, w, h, self.Color)
		end
	end

	function btn:UpdateColours( skin )
		if ( self:GetDisabled() )					then return self:SetTextStyleColor( skin.Colours.Button.Disabled ) end
		if ( self.Depressed || self.m_bSelected )	then return self:SetTextStyleColor(Color(80,80,80)) end
		if ( self.Hovered )							then return self:SetTextStyleColor(Color(140,140,140)) end
		return self:SetTextStyleColor(Color(120,120,120))
	end

	btn:SetFont("ScoreboardText")

	return btn
end

/*------------------------------------
	Rows
--------------------------------------*/
	
local function MakeRow(player)
	local rowParent = SSB.Content:Add("DPanel")
	rowParent:SetHeight(36)
	rowParent:Dock(TOP)

	local row = rowParent:Add("DButton")
	rowParent.row = row
	row:SetText("")

	row:SetHeight(36)
	row:Dock(TOP)
	row:DockMargin(0,0,0,0)

	row.Color = Config.MainRowColor
	row.TextColor = Config.TextColor
	if player then
		row.Color = team.GetColor(player:Team())
		//row.Color = HSVToColor(math.random(0,360),0.7,0.9) // DEBUG
		rowParent:DockMargin(unpack(Config.RowMargin))
		if Config.AutoTextColor then
			local a, b, c = ColorToHSV(row.Color)
			row.TextColor = HSVToColor(a, 0.7, 0.5)
		end
		player.SSB_Row = rowParent
	else
		rowParent:SetZPos(-9999)
	end

	function row:Paint(w, h)
		DrawBox(0, 0, w, h, row.Color)
	end

	row.Player = player

	// Player Avatar
	if Config.ShowAvatar then
		row.AvatarButton = row:Add( "DButton" )
		row.AvatarButton:DockMargin(2,2,2,2)
		row.AvatarButton:Dock(LEFT)
		row.AvatarButton:SetSize( 32, 32 )
		row.AvatarButton.DoClick = function() if IsValid(player) then row.Player:ShowProfile() end end
		row.AvatarButton.Paint = function() end
		row.AvatarButton:SetText("")

		if IsValid(player) then
			row.Avatar = vgui.Create( "AvatarImage", row.AvatarButton )
			row.Avatar:SetSize( 32, 32 )
			row.Avatar:SetMouseInputEnabled( false )
			row.Avatar:SetPlayer(row.Player, 64)

			function row.Avatar:PaintOver(w, h)
				surface.SetDrawColor(Color(row.Color.r*0.9,row.Color.g*0.9,row.Color.b*0.9))
				surface.DrawOutlinedRect(0, 0, w, h)
			end
		end
	end
	// Player Avatar

	// Player Name
	row.Name = row:Add( "DLabel" )
	row.Name:Dock( FILL )
	row.Name:SetFont( "ScoreboardText" )
	row.Name:DockMargin( 8, 0, 0, 0 )
	row.Name:SetText("Name")
	row.Name:SetColor(row.TextColor)
	// Player Name

	// Player Mute
	if Config.ShowMute then
		row.Mute = row:Add("DImageButton")
		row.Mute:DockMargin(2, 2, 2, 2)
		row.Mute:SetSize(32, 32)
		row.Mute:Dock(RIGHT)
	end
	// Player Mute

	// Player Ping
	if Config.ShowPing then
		row.Ping = row:Add( "DLabel" )
		row.Ping:Dock(RIGHT)
		row.Ping:SetFont( "ScoreboardText" )
		row.Ping:SetTextColor( Color( 255, 255, 255 ) )
		row.Ping:DockMargin( 8, 0, 0, 0 )
		row.Ping:SetText("Ping")
		row.Ping:SetContentAlignment( 5 )
		row.Ping:SetColor(row.TextColor)
	end
	// Player Ping

	// Total time
	if Config.ShowTotalTime then
		row.TotalTime = row:Add( "DLabel" )
		row.TotalTime:Dock(RIGHT)
		row.TotalTime:SetFont( "ScoreboardText" )
		row.TotalTime:SetTextColor( Color( 255, 255, 255 ) )
		row.TotalTime:DockMargin( 8, 0, 0, 0 )
		row.TotalTime:SetText("Total Time")
		row.TotalTime:SetContentAlignment( 5 )
		row.TotalTime:SetColor(row.TextColor)
		row.TotalTime:SetWidth(128)
	end
	// Total time

	// Player Deaths
	if Config.ShowDeaths then
		row.Deaths = row:Add( "DLabel" )
		row.Deaths:Dock(RIGHT)
		row.Deaths:SetFont( "ScoreboardText" )
		row.Deaths:SetTextColor( Color( 255, 255, 255 ) )
		row.Deaths:DockMargin( 8, 0, 0, 0 )
		row.Deaths:SetText("D")
		row.Deaths:SetContentAlignment( 5 )
		row.Deaths:SetWidth(32)
		row.Deaths:SetColor(row.TextColor)
	end
	// Player Deaths

	// Player Kills
	if Config.ShowKills then
		row.Kills = row:Add( "DLabel" )
		row.Kills:Dock(RIGHT)
		row.Kills:SetFont( "ScoreboardText" )
		row.Kills:SetTextColor( Color( 255, 255, 255 ) )
		row.Kills:DockMargin( 8, 0, 0, 0 )
		row.Kills:SetText("K")
		row.Kills:SetContentAlignment( 5 )
		row.Kills:SetWidth(64)
		row.Kills:SetColor(row.TextColor)
	end
	// Player Kills

	// Player Group
	if Config.ShowGroup then
		row.Group = row:Add( "DLabel" )
		row.Group:Dock(RIGHT)
		row.Group:SetFont( "ScoreboardText" )
		row.Group:SetTextColor( Color( 255, 255, 255 ) )
		row.Group:DockMargin( 8, 0, 0, 0 )
		row.Group:SetText("Rank")
		row.Group:SetContentAlignment( 5 )
		row.Group:SetWidth(180)
		row.Group:SetColor(row.TextColor)
	end
	// Player Group

	// Player Team 
	if Config.ShowMoney && DarkRP then
		row.Money = row:Add( "DLabel" )
		row.Money:Dock(RIGHT)
		row.Money:SetFont( "ScoreboardText" )
		row.Money:SetTextColor( Color( 255, 255, 255 ) )
		row.Money:DockMargin( 8, 0, 0, 0 )
		row.Money:SetText("Money")
		row.Money:SetContentAlignment( 5 )
		row.Money:SetWidth(120)
		row.Money:SetColor(row.TextColor)
	end
	// Player Team

	// Player Team
	if Config.ShowTeam then
		row.Team = row:Add( "DLabel" )
		row.Team:Dock(RIGHT)
		row.Team:SetFont( "ScoreboardText" )
		row.Team:SetTextColor( Color( 255, 255, 255 ) )
		row.Team:DockMargin( 8, 0, 0, 0 )
		row.Team:SetText(DarkRP and "Job" or "Team")
		row.Team:SetContentAlignment( 5 )
		row.Team:SetWidth(180)
		row.Team:SetColor(row.TextColor)
	end
	// Player Team

	function FormatTime(timetable, type)
		if type == 0 then
			return string.format("%02i:%02i:%02i", math.floor(timetable.h/24), timetable.h%24, timetable.m)
		elseif type == 1 then
			return string.format("%02i:%02i:%02i:%02i", math.floor(timetable.h/24), timetable.h%24, timetable.m, math.floor(timetable.s)%60)
		elseif type == 2 then
			if timetable.h > 24 then
				local days = math.floor(timetable.h/24)
				if days > 7 then
					return string.format("%02iw %02id %02ih %02im", math.floor(days/7), days%7, timetable.h%24, timetable.m)
				else
					return string.format("%02id %02ih %02im", days, timetable.h%24, timetable.m)
				end
			else
				return string.format("%02ih %02im", timetable.h, timetable.m)
			end
		end
	end


	function row:Think()
		if IsValid(player) then
			row.Color = team.GetColor(player:Team()) // DEBUG
			if Config.AutoTextColor then
				local a, b, c = ColorToHSV(row.Color)
				row.TextColor = HSVToColor(a, b*0.8, c-0.3)
				if b > 0.9 then
					row.TextColor = HSVToColor(a, b*0.8, c-0.5)
				elseif b < 0.2 then
					row.TextColor = HSVToColor(a, 0, c+0.8)
				elseif b < 0.7 then
					row.TextColor = HSVToColor(a, b*0.8, c+0.3)
				elseif math.abs((row.TextColor.r + row.TextColor.g + row.TextColor.b)/3 - (row.Color.r + row.Color.g + row.Color.b)/3)<20 then
					local a, b, c = ColorToHSV(row.Color)
					//row.TextColor = HSVToColor(a, 0.8, 1)
				end
			end

			self.Name:SetText(player:Name()); row.Name:SetColor(row.TextColor)
			if self.Ping then self.Ping:SetText(player:Ping()); self.Ping:SetColor(row.TextColor) end
			if self.Kills then self.Kills:SetText(player:Frags()); self.Kills:SetColor(row.TextColor) end
			if self.Deaths then self.Deaths:SetText(player:Deaths()); self.Deaths:SetColor(row.TextColor) end
			if self.Group then self.Group:SetText(Config.GroupAliases[player:GetUserGroup()] or FromCapital(player:GetUserGroup())); self.Group:SetColor(row.TextColor) end
			if self.Team then self.Team:SetText(team.GetName(player:Team())); self.Team:SetColor(row.TextColor) end
			if self.Money then self.Money:SetText(DarkRP.formatMoney(row.Player:getDarkRPVar("money"))); self.Money:SetColor(row.TextColor) end
			if self.TotalTime then self.TotalTime:SetText(FormatTime(string.FormattedTime((row.Player.GetUTimeTotalTime and row.Player:GetUTimeTotalTime() or 0)),0)); self.TotalTime:SetColor(row.TextColor) end

			if self.Mute && (self.muted == nil || self.muted != self.Player:IsMuted()) then
				self.muted = self.Player:IsMuted()
				if self.muted then
					self.Mute:SetImage("icon32/muted.png")
				else
					self.Mute:SetImage("icon32/unmuted.png")
				end
				self.Mute.DoClick = function() self.Player:SetMuted(!self.muted) end
			end
		end
	end

	// Opening row

	function row:DoClick()
		if !player then return end

		for k, v in pairs(SSB.Content:GetChildren()[1]:GetChildren()) do
			if v == rowParent then continue end
			v.opened = false
		end
		rowParent.opened = !rowParent.opened
		surface.PlaySound("buttons/lightswitch2.wav")
		if rowParent.opened then
			rowParent:Open()
		else
			timer.Simple(2,function()
				if IsValid(rowParent) && IsValid(rowParent.Content) && !rowParent.opened then
					rowParent.Content:Remove()
				end
			end)
		end
	end

	local ImportantGroups = {owner = 3, superadmin = 2, vip = 1}

	function rowParent:Think()	
		if IsValid(row.Player) then
			if self.zPos != row.Player:Team() then
				self:SetZPos(row.Player:Team() - (ImportantGroups[row.Player:GetUserGroup()] or 0) * 1000)	
			end
		end
		if row.Player && !IsValid(row.Player) then
			local children = SSB.Content:GetChildren()
			self:Remove()
			for k, v in pairs(children) do
				v:Dock(TOP)
			end
			return
		end
		rowParent:SetHeight(math.Clamp(Lerp(FrameTime()*10, rowParent:GetTall(), self.opened and 308 or 32), 36, 300))
	end

	function rowParent:Paint(w, h)
		DrawBox(0, 0, w, h, MulColor(row.Color, 0.8))
	end

	function rowParent:Open()
		if IsValid(rowParent.Content) then
			rowParent.Content:Remove()
		end

		local content = rowParent:Add("DPanel")
		rowParent.Content = content
		function content:Paint() end
		content:Dock(FILL)

		// Model panel
		local mdl = content:Add("DModelPanel")
		mdl:DockMargin(10, 10, 10, 10)
		mdl:SetWidth(200)
		mdl:Dock(LEFT)
		mdl:SetModel(row.Player:GetModel())
		function mdl:LayoutEntity(ent) return end
		mdl.OldPaint = mdl.Paint
		function mdl:Paint(w, h)
			if IsValid(row.Player)then
				if !mdl.disabled  then
					DrawBox(0, 0, w, h, row.Color)
					mdl:OldPaint()
					if Config.ShowHP then
						DrawBar(1, h-30, w-2, 29, "%", math.Clamp(row.Player:Health(),0,100), 100, Color(255,0,0))
					end
				else
					SpyMat:SetTexture("$basetexture", GetRenderTarget("ssbspycam", SSB.SpySizeX*2, SSB.SpySizeY*2, false))
					surface.SetMaterial(SpyMat)
					surface.DrawTexturedRect(0, 0, w, h)
				end
			end
		end
		function mdl.Entity:GetPlayerColor() return row.Player:GetPlayerColor() end 
		local size1, size2 = mdl.Entity:GetRenderBounds()
		local size = (Vector(math.abs(size1.x),math.abs(size1.y),math.abs(size1.z))+Vector(math.abs(size2.x),math.abs(size2.y),math.abs(size2.z))):Length()
		mdl:SetLookAt((size1+size2)/2)
		mdl:SetCamPos((size1+size2)/2 + Vector( 42, 30, 15 )*size*0.012)
		function mdl:DoClick()
			if Config.Spy && gb.is_mod(LocalPlayer()) then
				mdl.disabled = !mdl.disabled
				if !mdl.disabled then
					SSB.SpyPly = nil
				else
					SSB.SpyPly = row.Player
					SSB.SpySizeX, SSB.SpySizeY = mdl:GetSize()
				end
			end
		end

		// Button container
		local buttonCon = content:Add("DPanel")
		buttonCon:DockMargin(0,10,10,10)
		buttonCon:Dock(LEFT)
		buttonCon:SetWidth(300)
		function buttonCon:Paint() end

		// Buttons

		local btn = MakeButton(buttonCon)
		btn:DockMargin(0,0,0,0)
		btn:SetHeight(36)
		btn:Dock(TOP)
		btn:SetText("Open profile")
		function btn:DoClick()
			surface.PlaySound("buttons/button15.wav")
			row.Player:ShowProfile()
		end

		local btn = MakeButton(buttonCon)
		btn:DockMargin(0,2,0,0)
		btn:SetHeight(36)
		btn:Dock(TOP)
		btn:SetText("Copy SteamID")
		function btn:DoClick()
			surface.PlaySound("buttons/button15.wav")
			SetClipboardText(row.Player:SteamID())
			btn:SetText("Copied!")
			timer.Simple(2, function()
				if !IsValid(btn) then return end
				btn:SetText("Copy SteamID")
			end)
		end

		if ulx && gb.is_mod(LocalPlayer()) then
			local btn = MakeButton(buttonCon)
			btn:DockMargin(0,2,0,0)
			btn:SetHeight(36)
			btn:Dock(BOTTOM)
			btn:SetText("Slay")
			function btn:DoClick()
				surface.PlaySound("buttons/button15.wav")
				RunConsoleCommand("ulx", "slay", row.Player:Name())
			end

			local btn = MakeButton(buttonCon)
			btn:DockMargin(0,2,0,0)
			btn:SetHeight(36)
			btn:Dock(BOTTOM)
			btn:SetText("Teleport")
			function btn:DoClick()
				surface.PlaySound("buttons/button15.wav")
				RunConsoleCommand("ulx", "teleport", row.Player:Name())
			end
			

			local btn = MakeButton(buttonCon)
			btn:DockMargin(0,2,0,0)
			btn:SetHeight(36)
			btn:Dock(BOTTOM)
			btn:SetText("Go to")
			function btn:DoClick()
				surface.PlaySound("buttons/button15.wav")
				RunConsoleCommand("ulx", "goto", row.Player:Name())
			end
		end

		// Text Container
		local textCon = content:Add("DPanel")
		textCon:DockMargin(0,10,10,10)
		textCon:Dock(FILL)
		function textCon:Paint() end

		local str = ""

		if Config.Info.ShowHP then str = str .. "Health: " .. row.Player:Health() .. "%\n" end
		if Config.Info.ShowArmor then str = str .. "Armor: " .. row.Player:Armor() .. "%\n" end
		if row.Player.GetUTimeTotalTime then
			if Config.Info.ShowServerTime then str = str .. "Total time: " .. FormatTime(string.FormattedTime(row.Player.GetUTimeTotalTime and row.Player:GetUTimeTotalTime() or 0), Config.Info.TimeFormat) .. "\n" end
			if Config.Info.ShowSessionTime then str = str .. "Session time: " .. FormatTime(string.FormattedTime(row.Player.GetUTimeSessionTime and row.Player:GetUTimeSessionTime() or 0), Config.Info.TimeFormat) .. "\n" end
		end
		if Config.Info.ShowMoney && DarkRP then str = str .. "Money: " .. DarkRP.formatMoney(row.Player:getDarkRPVar("money")) .. "\n" end
		if Config.Info.ShowPoints && (row.Player.PS2_GetWallet || row.Player.PS_GetPoints) then str = str .. "Points: " .. (row.Player.PS2_GetWallet and (row.Player:PS2_GetWallet().points or 0) or (row.Player.PS_GetPoints and row.Player:PS_GetPoints() or 0) or 0) .. "\n" end

		for k, v in pairs(Config.Info:GetCustomText(row.Player) or { }) do
			str = str .. v .. "\n"
		end 

		// Text
		local text = textCon:Add("DLabel")
		text:SetFont("ScoreboardText")
		text:SetColor(Config.Info.TextColor)
		text:DockMargin(0,0,0,0)
		text:SetHeight(28)
		text:Dock(TOP)
		text:SetText(str)
		text:SizeToContentsY()
	end
end

/*------------------------------------
	Main panel creation
--------------------------------------*/

local function CreateMainPanel() 
	if IsValid(SSB) then
		SSB:Remove()
		SSB = nil
  	end

  	SSB = vgui.Create("DPanel")
  	SSB:MakePopup()
  	SSB:SetKeyboardInputEnabled(false)

	function SSB:Init()
		self:SetSize( math.Min(Config.Width, ScrW()), ScrH() - 200 )
		self:SetPos( ScrW() / 2 - math.Min(Config.Width, ScrW())/2, 100 )

		self.Header = self:Add( "Panel" )
		self.Header:Dock( TOP )
		self.Header:SetHeight( 100 )

		self.Name = self.Header:Add( "DLabel" )
		self.Name:SetFont( "ScoreboardTitle" )
		self.Name:SetTextColor(Config.TitleColor)
		self.Name:DockMargin(0,32,0,0)
		self.Name:Dock( TOP )
		self.Name:SetHeight( 60 )
		self.Name:SetContentAlignment( 2 )
		self.Name:SetText(Config.Title)

		self.Plys = self:Add( "DLabel" )
		self.Plys:SetFont( "ScoreboardTitleSmall" )
		self.Plys:SetTextColor(Config.TitleColor)
		//self.Plys:DockMargin(Config.InsideMargin,12,Config.InsideMargin,0)
		//self.Plys:Dock( BOTTOM )
		self.Plys:SetHeight( 30 )
		self.Plys:SetPos(0, 60+Config.InsideMargin)
		self.Plys:SetWidth(Config.Width-16)
		self.Plys:SetContentAlignment(3)
		self.Plys:SetText("Players: 0")

		/*
		self.Content = self:Add("DHTML")
		self.Content:DockMargin(Config.InsideMargin, Config.InsideMargin, Config.InsideMargin, Config.InsideMargin)
		self.Content:Dock(FILL)
		self.Content:SetHTML([[
			<video autoplay loopid="bgvid">
			<source src="http://dl.dropboxusercontent.com/u/73430511/ShareX/2015/04/Looping_Clouds.mp4" type="video/mp4" style="position: fixed; right: 0; bottom: 0;min-width: 100%; min-height: 100%;width: auto; height: auto; z-index: -100;" />
			</video>
		]])
		*/

		self.Content = self:Add("DScrollPanel")
		self.Content:DockMargin(Config.InsideMargin, Config.InsideMargin, Config.InsideMargin, Config.InsideMargin)
		self.Content:Dock(FILL)
		function self.Content:Paint(w, h)
			if Config.InsideColor.a == 0 then return end
			DrawBox(0, 0, w, h, Config.InsideColor)
		end

		local vbar = self.Content:GetVBar()

		function vbar:Paint(w, h)
			DrawBox(0, 0, w, h, Config.InsideColor)
		end

		function vbar.btnUp:Paint(w, h)
			DrawBox(0, 0, w, h, Config.MainRowColor)
		end

		function vbar.btnDown:Paint(w, h)
			DrawBox(0, 0, w, h, Config.MainRowColor)
		end

		function vbar.btnGrip:Paint(w, h)
			DrawBox(0, 0, w, h, Config.MainRowColor)
		end

		local header_row = MakeRow()

		for k, v in pairs(player.GetAll()) do
			MakeRow(v)
		end
	end

	function SSB:Paint(w, h)
		DrawBox(0, 0, w, h, Config.MainColor)
	end

    SSB:Init()
end


timer.Create("SSBScan", 2, 0, function()
	if IsValid(SSB) then
		for k, v in pairs(player.GetAll()) do
			if IsValid(v.SSB_Row) then continue end
			MakeRow(v)
		end

		SSB.Plys:SetText("Players: " .. #player.GetAll() .. "/" .. game.MaxPlayers())
	end
end)

/*------------------------------------
	Scoreboard show/hide
--------------------------------------*/

local function ShowScoreboard() 
	//if IsValid(SSB) then
	//	SSB:SetVisible(true)
	//else
		CreateMainPanel()
	//end
end

local function HideScoreboard() 
	//if IsValid(SSB) then
		//SSB:SetVisible(false)
		SSB:Remove()
		//SSB = nil
  // end
end
	
/*----------------------	--------------
	SpyCam rendering
--------------------------------------*/

hook.Add( "RenderScene", "SSBRenderScene", function(origin, ang)
	if !IsValid(SSB) || !SSB.SpyPly || !IsValid(SSB.SpyPly) then return end

	local oldRT = render.GetRenderTarget()
	render.SetRenderTarget(GetRenderTarget("ssbspycam", SSB.SpySizeX*2, SSB.SpySizeY*2, false))
	render.Clear(0, 0, 0, 255)
	render.ClearDepth()
	render.ClearStencil()

	local ang = Angle(30,SSB.SpyPly:EyeAngles().yaw,0)

	local tr = util.TraceLine({
		start = SSB.SpyPly:GetPos(), 
		endpos = SSB.SpyPly:GetPos() - (ang:Forward()*100), 
		filter = function(ent) if ( ent == SSB.SpyPly ) then return false end end
	})

	local pos = SSB.SpyPly:GetPos() + Vector(0,0,60) - (ang:Forward()*70) * tr.Fraction

	render.RenderView({	
		x = 0,
		y = 0,
		w = SSB.SpySizeX*2,
		h = SSB.SpySizeY*2,
		origin = pos,
		angles = ang,
		drawpostprocess = true,
		drawhud = false,
		drawmonitors = false,
		drawviewmodel = false,
		fov = 110
	})

	render.SetRenderTarget(oldRT)
end)

/*------------------------------------
	Scoreboard hooks
--------------------------------------*/

SpyMat = CreateMaterial(
	"UnlitGeneric",
	"GMODScreenspace",
	{
		["$basetexturetransform"] = "center .5 .5 scale -1 -1 rotate 0 translate 0 0",
		["$texturealpha"] = "0",
		["$vertexalpha"] = "1",
	})

local function RegisterHooks()
	if GAMEMODE then
		function GAMEMODE:ScoreboardShow()
			ShowScoreboard() 
		end

		function GAMEMODE:ScoreboardHide()
			HideScoreboard() 
		end
	end

	if GM then
		function GM:ScoreboardShow()
			ShowScoreboard() 
		end

		function GM:ScoreboardHide()
			HideScoreboard() 
		end
	end

	SpyMat = CreateMaterial(
	"UnlitGeneric",
	"GMODScreenspace",
	{
		["$basetexturetransform"] = "center .5 .5 scale -1 -1 rotate 0 translate 0 0",
		["$texturealpha"] = "0",
		["$vertexalpha"] = "1",
	})

	if DarkRP then
		hook.Add("ScoreboardShow", "FAdmin_scoreboard", function()
			ShowScoreboard()
			return false
		end)

		hook.Add("ScoreboardHide", "FAdmin_scoreboard", function()
			HideScoreboard() 
		end)
	end
end

RegisterHooks()

hook.Add("InitPostEntity", "SSHooks", RegisterHooks)
hook.Add("DarkRPFinishedLoading", "SSRpHooks", RegisterHooks)

hook.Add("Tick", "SSConfigUpdate", function()
	if Config != SlimScoreboardConfig then
		Config = SlimScoreboardConfig
		CreateMainPanel()
	end
end)

//CreateMainPanel() // DEBUG