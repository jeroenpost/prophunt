/*-----------------------------------------------------------
	Slim Scoreboard
	
	Copyright © 2015 Szymon (Szymekk) Jankowski
	All Rights Reserved
	Steam: https://steamcommunity.com/id/szymski
-------------------------------------------------------------*/

SlimScoreboardConfig = { }
local Config = SlimScoreboardConfig

/*----------------------------------------------
	Slim Scoreboard Configuration
------------------------------------------------*/

Config.Width 								= 1000

Config.Title 								= "TTT-FUN Prophunt"				// The title

Config.BoxStyle								= 0

Config.MainColor							= Color(52,52,52,255)			// Main color
Config.TitleColor							=  Color(220,220,0,255)

Config.InsideColor							= Color(255, 255, 255)			// Color of the box containing rows (change)
Config.InsideMargin							= 16							// Margin of the box containing rows

Config.MainRowColor							= Color(80, 80, 80)
Config.RowMargin							= {2, 2, 2, 0}
Config.TextColor 							= Color(255,255,255)
Config.AutoTextColor						= true					
														
/*------------------------------------
	Columns
--------------------------------------*/

Config.ShowAvatar							= true	
Config.ShowTeam								= true							// Job in DarkRP					
Config.ShowMoney							= true												
Config.ShowGroup							= true
Config.ShowKills							= true
Config.ShowDeaths							= true
Config.ShowPing								= true
Config.ShowMute								= true
Config.ShowTotalTime						= true							// Needs UTime

Config.GroupAliases							= {
	superadmin	= "Super Admin",
	vip			= "VIP"
}

/*------------------------------------
	Player
--------------------------------------*/

Config.AdminGroups							= {								// These groups will be able to use ulx buttons
	"superadmin",
	"owner"
}

Config.ShowHP								= true
Config.Spy 									= true							// When true, admins can click on player model and see what he's doing
																													
/*------------------------------------
	Player Information
--------------------------------------*/

Config.Info = { }

Config.Info.TextColor 						= Color(235,235,235)

Config.Info.ShowHP							= true
Config.Info.ShowArmor						= true

Config.Info.TimeFormat						= 0								// 0 - dd:hh:mm, 1 - dd:hh:mm:ss, 2 - "0d 0h 0m"
Config.Info.ShowServerTime					= true							// Requires UTime
Config.Info.ShowSessionTime					= true							// Requires UTime

Config.Info.ShowMoney						= true		
Config.Info.ShowPoints						= true							// Pointshop (1 or 2) points 

// A function for displaying custom text. You need to return tables. One table - one line
function Config.Info:GetCustomText(ply)
	//return {"I like trains - " .. ply:Name(), "Visit: szymekk.info"}
end