CATEGORY.Name = 'Megadonator Playermodels'
CATEGORY.Desc = 'Playermodels for Mega Donators'
CATEGORY.Icon = 'key'
CATEGORY.NotAllowedUserGroups = { "user","member","donator", "demotedmember","", "jr_mod", "jr_admin" }
CATEGORY.Order = 4
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "playermodels"
CATEGORY.buyWhenDeath = true
function CATEGORY:CanPlayerBuy(ply) return gb.is_megadonator(ply) end
