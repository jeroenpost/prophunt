ITEM.Name = 'Extra Grenade'
ITEM.Price = 250
ITEM.Model = "models/Items/AR2_Grenade.mdl"
ITEM.WeaponClass = 'item_ar2_grenade'
ITEM.SingleUse = true


function ITEM:CanPlayerBuy( ply )
    
    local Class = ply:GetPlayerClass()
    if ( not Class ) then return false end

    if ply.boughtGrenade and ply.boughtGrenade > CurTime() then
        return false,"You can only buy 1 grenade per 3 minutes. "..math.floor(ply.boughtGrenade - CurTime()).." seconds left"
    end

    if Class.DisplayName == "Hunter" then 
        return true
    else
        return false,"You must be a hunter to buy weapons"
    end



    return false

end

function ITEM:OnBuy(ply)
    
	 ply:Give(self.WeaponClass)
     ply.boughtGrenade = CurTime() + 200
	--ply:SelectWeapon(self.WeaponClass)
end

