CATEGORY.Name = 'Taunts'
CATEGORY.Icon = 'sound'
CATEGORY.id = "sounds"
CATEGORY.Desc = 'Collect taunts'

CATEGORY.Order = 5
CATEGORY.CanPlayerSee = function() return true end
CATEGORY.buyWhenDeath = true