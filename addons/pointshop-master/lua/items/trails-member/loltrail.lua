ITEM.Name = 'LOL Trail'
ITEM.Price = 150
ITEM.Material = 'trails/lol.vmt'

function ITEM:OnEquip(ply, modifications)
    local Class = ply:GetPlayerClass()
    if ( not Class ) then return false end
    if Class.DisplayName ~= "Hunter" then return end
	ply.TrailPSLOL = util.SpriteTrail(ply, 0, modifications.color, false, 15, 1, 4, 0.125, self.Material)
end

function ITEM:OnHolster(ply)
	SafeRemoveEntity(ply.TrailPSLOL)
end

function ITEM:Modify(modifications)
	PS:ShowColorChooser(self, modifications)
end

function ITEM:OnModify(ply, modifications)
	SafeRemoveEntity(ply.TrailPSLOL)
	self:OnEquip(ply, modifications)
end
