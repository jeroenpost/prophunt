CATEGORY.Name = 'Member Trails'
CATEGORY.Desc = 'Trails for members. 10 hours of playtime makes you a member'
CATEGORY.Icon = 'key'
CATEGORY.Order = 1
CATEGORY.NotAllowedUserGroups = { "user","", "demotedmember"}
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "trials"
CATEGORY.CanPlayerSee = function() return true end