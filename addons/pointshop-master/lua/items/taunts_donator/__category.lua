CATEGORY.Name = 'Donator Taunts'
CATEGORY.Desc = 'Taunts for Donators. Donators only pay $2.5/mo!'
CATEGORY.Icon = 'key'
CATEGORY.NotAllowedUserGroups = { "user","member", "demotedmember","", "jr_mod", "jr_admin" }
CATEGORY.Order = 3
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "sounds"
CATEGORY.buyWhenDeath = true
function CATEGORY:CanPlayerBuy(ply) return gb.is_donator(ply) end