CATEGORY.Name = 'DonatorPlus Propmodels'
CATEGORY.Desc = 'Playermodels for Donator Plus'
CATEGORY.Icon = 'key'
CATEGORY.NotAllowedUserGroups = { "user","member", "demotedmember","", "jr_mod", "jr_admin" }
CATEGORY.Order = 3
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "playermodels"
CATEGORY.buyWhenDeath = true
function CATEGORY:CanPlayerBuy(ply) return gb.is_donator(ply) end
