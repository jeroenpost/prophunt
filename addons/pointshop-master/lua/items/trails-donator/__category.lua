CATEGORY.Name = 'Donator Trails'
CATEGORY.Desc = 'Donators can buy these trails'
CATEGORY.Icon = 'key'
CATEGORY.Order = 2
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "trials"
CATEGORY.CanPlayerSee = function() return true end
function CATEGORY:CanPlayerBuy(ply) return gb.is_donator(ply) end
