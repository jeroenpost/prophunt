ITEM.Name = 'Brody Quest 1'
ITEM.Price = 250
ITEM.Material = "vgui/taunts.png"
ITEM.Taunt = 'brodyquest1'
ITEM.buyWhenDeath = true ITEM.SingleUse = false


function ITEM:OnEquip(ply)
    table.insert(ply.taunts, "taunts/"..self.Taunt..".mp3")
end

function ITEM:OnHolster(ply)
    self:OnSell(ply)
end

function ITEM:OnSell(ply)
	 table.RemoveByValue(ply.taunts, "taunts/"..self.Taunt..".mp3")
end