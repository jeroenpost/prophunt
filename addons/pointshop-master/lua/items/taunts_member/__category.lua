CATEGORY.Name = 'Member Taunts'
CATEGORY.Desc = 'Taunts for members. A member is someone who played more then 10 hours'
CATEGORY.Icon = 'key'
CATEGORY.NotAllowedUserGroups = { "user", "demotedmember","", "jr_mod", "jr_admin" }
CATEGORY.Order = 2
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "sounds"
CATEGORY.buyWhenDeath = true