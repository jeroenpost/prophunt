CATEGORY.Name = 'Superdonator Playermodels'
CATEGORY.Desc = 'Playermodels for Super Donators'
CATEGORY.Icon = 'key'
CATEGORY.NotAllowedUserGroups = { "user","member","donator", "demotedmember","", "jr_mod", "jr_admin" }
CATEGORY.Order = 4
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "playermodels"
CATEGORY.buyWhenDeath = true
function CATEGORY:CanPlayerBuy(ply) return gb.is_superdonator(ply) end
