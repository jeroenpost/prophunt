ITEM.Name = 'Sceptile'
ITEM.Price = 100000
ITEM.Model =  "models/player/custom/Sceptile/Sceptile_pm.mdl"
ITEM.AdminOnly = false

function ITEM:OnEquip(ply, modifications)
	if not ply._OldModel then
		ply._OldModel = ply:GetModel()
	end
	timer.Simple(1, function() ply:SetModel(self.Model) end)
end

function ITEM:OnHolster(ply)
	if ply._OldModel then
		ply:SetModel(ply._OldModel)
	end
end