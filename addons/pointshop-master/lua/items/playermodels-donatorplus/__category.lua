CATEGORY.Name = 'Donator+ Playermodels'
CATEGORY.Desc = 'Playermodels for Donator +. '
CATEGORY.Icon = 'key'
CATEGORY.NotAllowedUserGroups = { "user","member","donator", "demotedmember","", "jr_mod", "jr_admin" }
CATEGORY.Order = 3
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "playermodels"
CATEGORY.buyWhenDeath = true
function CATEGORY:CanPlayerBuy(ply) return gb.is_donatorplus(ply) end
