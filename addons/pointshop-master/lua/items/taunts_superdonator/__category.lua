CATEGORY.Name = 'Superdonator Taunts'
CATEGORY.Desc = 'Taunts for Super Donators. A Super Donator donated $50 or more'
CATEGORY.Icon = 'key'
CATEGORY.Order = 6
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "sounds"
CATEGORY.buyWhenDeath = true
function CATEGORY:CanPlayerBuy(ply) return gb.is_superdonator(ply) end
