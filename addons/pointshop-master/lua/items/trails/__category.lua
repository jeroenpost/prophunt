CATEGORY.Name = 'Trails'
CATEGORY.Desc = 'Leave a trail behind you when you are walking'
CATEGORY.Icon = 'rainbow'
CATEGORY.Order = 3
CATEGORY.id = "trials"
--CATEGORY.IsSubcategory = true
--CATEGORY.SubcategoryChild = "hats"
CATEGORY.CanPlayerSee = function() return true end