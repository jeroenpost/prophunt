ITEM.Name = 'Rainbow Trail'
ITEM.Price = 1500
ITEM.Material = 'trails/rainbow.vmt'

function ITEM:OnEquip(ply, modifications)
    local Class = ply:GetPlayerClass()
    if ( not Class ) then return false end
    if Class.DisplayName ~= "Hunter" then return end
	ply.TrailPSRainBow = util.SpriteTrail(ply, 0, modifications.color, false, 15, 1, 4, 0.125, self.Material)
end

function ITEM:OnHolster(ply)
	SafeRemoveEntity(ply.TrailPSRainBow)
end

function ITEM:Modify(modifications)
	PS:ShowColorChooser(self, modifications)
end

function ITEM:OnModify(ply, modifications)
	SafeRemoveEntity(ply.TrailPSRainBow)
	self:OnEquip(ply, modifications)
end
