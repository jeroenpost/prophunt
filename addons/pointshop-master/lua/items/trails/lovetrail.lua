ITEM.Name = 'Love Trail'
ITEM.Price = 1500
ITEM.Material = 'trails/love.vmt'

function ITEM:OnEquip(ply, modifications)
    local Class = ply:GetPlayerClass()
    if ( not Class ) then return false end
    if Class.DisplayName ~= "Hunter" then return end
	ply.TrailPSLove = util.SpriteTrail(ply, 0, modifications.color, false, 15, 1, 4, 0.125, self.Material)
end

function ITEM:OnHolster(ply)
	SafeRemoveEntity(ply.TrailPSLove)
end

function ITEM:Modify(modifications)
	PS:ShowColorChooser(self, modifications)
end

function ITEM:OnModify(ply, modifications)
	SafeRemoveEntity(ply.TrailPSLove)
	self:OnEquip(ply, modifications)
end
