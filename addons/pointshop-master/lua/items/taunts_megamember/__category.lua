CATEGORY.Name = 'MegaMember Taunts'
CATEGORY.Desc = 'Taunts for megamembers. A member is someone who played more then 10 hours'
CATEGORY.Icon = 'key'
function CATEGORY:CanPlayerBuy(ply) return gb.IsMegaMember(ply) end
CATEGORY.Order = 2
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "sounds"
CATEGORY.buyWhenDeath = true