CATEGORY.Name = 'Prop Models - Donator'
CATEGORY.Icon = 'user_edit'
CATEGORY.id = "playermodels2"
CATEGORY.Desc = 'Change your prop T-pose Model'

CATEGORY.Order = 3
CATEGORY.CanPlayerSee = function() return true end
CATEGORY.buyWhenDeath = true
function CATEGORY:CanPlayerBuy(ply) return gb.is_donator(ply) end