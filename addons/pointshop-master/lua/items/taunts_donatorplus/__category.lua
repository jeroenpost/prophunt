CATEGORY.Name = 'Donator+ Taunts'
CATEGORY.Desc = 'Taunts for Donators+. A donator + is someone who donated 25 or more!'
CATEGORY.Icon = 'key'
CATEGORY.Order = 4
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "sounds"
CATEGORY.buyWhenDeath = true
function CATEGORY:CanPlayerBuy(ply) return gb.is_donatorplus(ply) end
