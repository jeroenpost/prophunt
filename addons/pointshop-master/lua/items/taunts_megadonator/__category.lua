CATEGORY.Name = 'Megadonator Taunts'
CATEGORY.Desc = 'Taunts for Mega Donators'
CATEGORY.Icon = 'key'
CATEGORY.Order = 7
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "sounds"
CATEGORY.buyWhenDeath = true
function CATEGORY:CanPlayerBuy(ply) return gb.is_megadonator(ply) end
