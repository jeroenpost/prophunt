local PANEL = {}

function PANEL:Init()
	self:SetTitle("PointShop Color Chooser")
	self:SetSize(300, 300)
	
	self:SetBackgroundBlur(true)
	self:SetDrawOnTop(true)
	
	self.colorpicker = vgui.Create('DColorMixer', self)
	--colorpicker:DockMargin(0, 0, 0, 60)
	self.colorpicker:Dock(FILL)
	
	local done = vgui.Create('DButton', self)
	done:DockMargin(0, 5, 0, 0)
	done:Dock(BOTTOM)
	
	done:SetText('')
	
	done.DoClick = function()
		self.OnChoose(self.colorpicker:GetColor())
		self:Close()
	end
	
	done.Paint = function( s, w, h )
		draw.RoundedBox( 0, 0, 0, w, h, Color(123, 227, 149))
		draw.RoundedBox( 0, 1, 1, w-2, h-2, Color(77, 209, 110))
		draw.SimpleText("DONE", 'PS_CatName', w/2, h/2, Color(255,255,255,150), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
	
	self:Center()
	self:Show()
	self.btnClose.Paint = function( s, w, h )
		Derma_DrawBackgroundBlur( s, CurTime() )
		draw.RoundedBox( 0, 0, 0, w, h-10, Color(219, 105, 105))
		draw.RoundedBox( 0, 1, 1, w-2, h-12, Color(232, 90, 90))
		draw.SimpleText("CLOSE", 'PS_CatName', w/2, h/2-5, Color(255,255,255,150), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
	self.btnMaxim.Paint = function( s, w, h ) end
	self.btnMinim.Paint = function( s, w, h ) end
	self.lblTitle:SetTextColor(color_white)
end

function PANEL:OnChoose(color)
	-- nothing, gets over-ridden
end

function PANEL:Paint( w, h )
	draw.RoundedBox( 0, 0, 0, w, h, Color(57, 61, 72))
	draw.RoundedBox( 0, 1, 1, w-2, h-2, Color(255, 255, 255, 10))
	draw.RoundedBox( 0, 2, 2, w-4, h-4, Color(57, 61, 72))
end

function PANEL:SetColor(color)
	self.colorpicker:SetColor(color or Color(255, 255, 255, 255))
end

vgui.Register('DPointShopColorChooser', PANEL, 'DFrame')
