player_manager.AddValidModel( "Sceptile", "models/player/custom/Sceptile/Sceptile_pm.mdl" )

player_manager.AddValidHands( "Sceptile", "models/player/custom/Sceptile/Sceptile viewhands.mdl", 0, "00000000" )

list.Set( "PlayerOptionsModel", "Sceptile", "models/player/custom/Sceptile/Sceptile_pm.mdl" )

player_manager.AddValidModel( "Sceptile SHINY", "models/player/custom/Sceptile/SHINY Sceptile_pm.mdl" )

player_manager.AddValidHands( "Sceptile SHINY", "models/player/custom/Sceptile/SHINY Sceptile viewhands.mdl", 0, "00000000" )

list.Set( "PlayerOptionsModel", "Sceptile SHINY", "models/player/custom/Sceptile/SHINY Sceptile_pm.mdl" )