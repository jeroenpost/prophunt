--[[

Hilda (Pokemon) Playermodel

Playermodel, Rigging to ValveBiped, modifications, qc file, lua, etc.
by LibertyForce (http://steamcommunity.com/id/libertyforce)

Ragdoll ported to Garry's Mod:
by 1337gamer15 (http://steamcommunity.com/profiles/76561198041663348)
>> Thanks to him, for the great MMD misc pack.

Original Hilda Model for MMD
by Darkliger01 (http://darkliger01.deviantart.com/)
>> Check out the awesome MMD models.

Thanks to CaptainBigButt for his proportions fix
(http://steamcommunity.com/id/CaptainBigButt)

]]--


-- Player Model

player_manager.AddValidModel( "Hilda", "models/hilda/hilda_player.mdl" )
player_manager.AddValidHands( "Hilda", "models/hilda/viewmodel/c_arms.mdl", 0, "00000000" )


-- NPC

local NPC =
{
	Name = "Hilda",
	Class = "npc_citizen",
	KeyValues =	{ citizentype = 4 },
	Model = "models/hilda/hilda_npc.mdl",
	Health = "200",
	Category = "Pokémon"
}
list.Set( "NPC", "npc_hilda", NPC )


-- Additional scaling

local LIFO = {}

CreateConVar( "lf_model_allowscale", 0, FCVAR_ARCHIVE )

function LIFO.HildaScale(ply)
	if SERVER then
		if ply and ply:IsValid() then
			timer.Simple(0.5, function()
				if ply:GetModel() == "models/hilda/hilda_player.mdl" then
					local Scale = 0.98
					local Eyes = 0.96
					ply:SetViewOffset( Vector( 0, 0, 64 * Eyes ) )
					ply:SetViewOffsetDucked( Vector( 0, 0, 28 * Eyes ) )
					ply:SetStepSize( 18 * Scale )
					ply:SetHull( Vector( -16 * Scale, -16 * Scale, 00 * Scale ), Vector(  16 * Scale,  16 * Scale, 72 * Scale ) )
					ply:SetHullDuck( Vector( -16 * Scale, -16 * Scale, 00 * Scale ), Vector(  16 * Scale,  16 * Scale, 36 * Scale ) )
				end
			end)
		end
	end
end
if GetConVarNumber("lf_model_allowscale") == 1 then hook.Add("PlayerSpawn", "Hilda Scale", LIFO.HildaScale) end

function LIFO.HildaResetScale(ply)
	if SERVER then
		if ply and ply:IsValid() then
			if ply:GetModel() == "models/hilda/hilda_player.mdl" then
				local Scale = 1
				ply:SetViewOffset( Vector( 0, 0, 64 * Scale ) )
				ply:SetViewOffsetDucked( Vector( 0, 0, 28 * Scale ) )
				ply:SetStepSize( 18 * Scale )
				ply:SetHull( Vector( -16 * Scale, -16 * Scale, 00 * Scale ), Vector(  16 * Scale,  16 * Scale, 72 * Scale ) )
				ply:SetHullDuck( Vector( -16 * Scale, -16 * Scale, 00 * Scale ), Vector(  16 * Scale,  16 * Scale, 36 * Scale ) )
			end
		end
	end
end
if GetConVarNumber("lf_model_allowscale") == 1 then hook.Add("PlayerDeath", "Hilda Scale Reset", LIFO.HildaResetScale) end