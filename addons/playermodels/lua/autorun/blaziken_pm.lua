player_manager.AddValidModel( "Blaziken", "models/player/custom/Blaziken/Blaziken_pm.mdl" )

player_manager.AddValidHands( "Blaziken", "models/player/custom/Blaziken/Blaziken viewhands.mdl", 0, "00000000" )

list.Set( "PlayerOptionsModel", "Blaziken", "models/player/custom/Blaziken/Blaziken_pm.mdl" )

player_manager.AddValidModel( "Blaziken SHINY", "models/player/custom/Blaziken/SHINY Blaziken_pm.mdl" )

player_manager.AddValidHands( "Blaziken SHINY", "models/player/custom/Blaziken/SHINY Blaziken viewhands.mdl", 0, "00000000" )

list.Set( "PlayerOptionsModel", "Blaziken SHINY", "models/player/custom/Blaziken/SHINY Blaziken_pm.mdl" )