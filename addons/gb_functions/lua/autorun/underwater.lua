-- Props underwater should die
if SERVER then
    last_hurt_interval = 0
    hook.Add("Think", "PH_DePort_Think", function()
        if last_hurt_interval + 1 < CurTime() then
            for _, pl in pairs(team.GetPlayers(TEAM_PROPS)) do
                if pl and pl:WaterLevel() >= 2 then
                    pl:SetHealth(pl:Health() - 5)
                    if pl:Alive() and pl:Health() < 1 then
                        pl:Kill()
                    end
                end
            end
            last_hurt_interval = CurTime()
        end
    end)
end